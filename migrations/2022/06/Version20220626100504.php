<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\Ticket\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220626100504 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $data = [
            'additional_privilege' => serialize([
                BaseControl::PRIVILEGE_MANIPULATION,
            ]),
            'name'                 => BaseControl::RESOURCE,
        ];
        $this->addSql('UPDATE core_role_resource SET additional_privilege = :additional_privilege WHERE name = :name', $data);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
