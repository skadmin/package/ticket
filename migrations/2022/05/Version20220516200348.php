<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220516200348 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket ADD created_at DATETIME NOT NULL, ADD last_update_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket_event ADD created_at DATETIME NOT NULL, ADD last_update_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket_type ADD created_at DATETIME NOT NULL, ADD last_update_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket DROP created_at, DROP last_update_at');
        $this->addSql('ALTER TABLE ticket_event DROP created_at, DROP last_update_at');
        $this->addSql('ALTER TABLE ticket_type DROP created_at, DROP last_update_at');
    }
}
