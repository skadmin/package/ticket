<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220516194030 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket CHANGE description perex LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE ticket_type ADD ticket_event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ticket_type ADD CONSTRAINT FK_BE054211F49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_BE054211F49E140A ON ticket_type (ticket_event_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket CHANGE perex description LONGTEXT CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_unicode_ci`');
        $this->addSql('ALTER TABLE ticket_type DROP FOREIGN KEY FK_BE054211F49E140A');
        $this->addSql('DROP INDEX IDX_BE054211F49E140A ON ticket_type');
        $this->addSql('ALTER TABLE ticket_type DROP ticket_event_id');
    }
}
