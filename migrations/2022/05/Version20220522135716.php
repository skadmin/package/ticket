<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220522135716 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ticket_event_mail_template (id INT AUTO_INCREMENT NOT NULL, ticket_event_id INT DEFAULT NULL, subject VARCHAR(255) NOT NULL, preheader VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, class VARCHAR(255) NOT NULL, recipients LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', parameters LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_39EA546EF49E140A (ticket_event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket_event_mail_template_rel_file (ticket_event_mail_template_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_D5DF19A7BA00B015 (ticket_event_mail_template_id), INDEX IDX_D5DF19A793CB796C (file_id), PRIMARY KEY(ticket_event_mail_template_id, file_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket_event_payment (id INT AUTO_INCREMENT NOT NULL, payment_id INT DEFAULT NULL, ticket_event_id INT DEFAULT NULL, price INT NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, INDEX IDX_A9BB813E4C3A3BB (payment_id), INDEX IDX_A9BB813EF49E140A (ticket_event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket_order (id INT AUTO_INCREMENT NOT NULL, ticket_event_id INT DEFAULT NULL, payment_id INT DEFAULT NULL, order_number VARCHAR(255) NOT NULL, total_price INT NOT NULL, paid_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, state INT NOT NULL, surname VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, note LONGTEXT NOT NULL, note_private LONGTEXT NOT NULL, INDEX IDX_DD19F013F49E140A (ticket_event_id), INDEX IDX_DD19F0134C3A3BB (payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ticket_order_item (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, ticket_id INT DEFAULT NULL, amount INT NOT NULL, price INT NOT NULL, applied_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, state INT NOT NULL, type VARCHAR(255) NOT NULL, hash VARCHAR(255) NOT NULL, INDEX IDX_C4798D5D8D9F6D38 (order_id), INDEX IDX_C4798D5D700047D2 (ticket_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ticket_event_mail_template ADD CONSTRAINT FK_39EA546EF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_event_mail_template_rel_file ADD CONSTRAINT FK_D5DF19A7BA00B015 FOREIGN KEY (ticket_event_mail_template_id) REFERENCES ticket_event_mail_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_event_mail_template_rel_file ADD CONSTRAINT FK_D5DF19A793CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_event_payment ADD CONSTRAINT FK_A9BB813E4C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_event_payment ADD CONSTRAINT FK_A9BB813EF49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_order ADD CONSTRAINT FK_DD19F013F49E140A FOREIGN KEY (ticket_event_id) REFERENCES ticket_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_order ADD CONSTRAINT FK_DD19F0134C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_order_item ADD CONSTRAINT FK_C4798D5D8D9F6D38 FOREIGN KEY (order_id) REFERENCES ticket_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ticket_order_item ADD CONSTRAINT FK_C4798D5D700047D2 FOREIGN KEY (ticket_id) REFERENCES ticket (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ticket_event_mail_template_rel_file DROP FOREIGN KEY FK_D5DF19A7BA00B015');
        $this->addSql('ALTER TABLE ticket_order_item DROP FOREIGN KEY FK_C4798D5D8D9F6D38');
        $this->addSql('ALTER TABLE ticket_order_item DROP FOREIGN KEY FK_C4798D5D700047D2');
        $this->addSql('DROP TABLE ticket_event_mail_template');
        $this->addSql('DROP TABLE ticket_event_mail_template_rel_file');
        $this->addSql('DROP TABLE ticket_event_payment');
        $this->addSql('DROP TABLE ticket_order');
        $this->addSql('DROP TABLE ticket_order_item');
    }
}
