<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Gateway;

use Nette\Utils\Strings;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationTeam;
use Skadmin\TournamentGame\Doctrine\Registration\RegistrationUser;
use SkadminUtils\Gateway\Transaction\AGatewayTransaction;
use SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransaction;
use SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransactionItem;

use function class_exists;
use function sprintf;

class BarionOrder
{
    public static function self(AGatewayTransaction $gatewayTransaction): AGatewayTransaction
    {
        return $gatewayTransaction;
    }

    public static function createFromOrder(TicketOrder $order): ?AGatewayTransaction
    {
        if (class_exists('\SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransaction')) {

            $items = [];
            foreach ($order->getGroupedItems() as $groupedItem) {
                $item    = $groupedItem->getItem();
                $items[] = new BarionGatewayTransactionItem(
                    $item->getName(),
                    $item->getTicket()->getTicketType()->getName(),
                    $groupedItem->getAmount(),
                    'x',
                    $item->getPrice(),
                    $item->getTicket()->getCode()
                );
            }

            foreach ($order->getItems([TicketOrderItem::TypePayment]) as $item) {
                $items[] = new BarionGatewayTransactionItem(
                    $item->getName(),
                    'Platba',
                    $item->getAmount(),
                    'x',
                    $item->getPrice(),
                    'payment'
                );
            }

            return new BarionGatewayTransaction($order->getEmail(), $order->getPhone(), $order->getOrderNumber(), $items);
        }

        return null;
    }
}
