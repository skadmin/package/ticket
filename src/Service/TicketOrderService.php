<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Service;

use App\Model\System\Flash;
use Endroid\QrCode\Logo\Logo;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Template;
use Nette\Application\UI\TemplateFactory;
use Nette\Http\FileUpload;
use Nette\Utils\Strings;
use Skadmin\FileStorage\FilePreview;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\TicketEventMailTemplate\TicketEventMailTemplateFacade;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Mail\CMailTicketOrder;
use Skadmin\Ticket\Mail\CMailTicketOrderTickets;
use Skadmin\Ticket\Mail\CMailTicketOrderTicketsBeforeEvent;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\SystemDir;

class TicketOrderService
{

    private Translator      $translator;
    private MailService     $mailService;
    private FileStorage     $fileStorage;
    private TemplateFactory $templateFactory;
    private SystemDir       $systemDir;
    private LinkGenerator   $linkGenerator;

    public function __construct(Translator $translator, MailService $mailService, FileStorage $fileStorage, TemplateFactory $templateFactory, SystemDir $systemDir, LinkGenerator $linkGenerator)
    {
        $this->translator      = $translator;
        $this->mailService     = $mailService;
        $this->fileStorage     = $fileStorage;
        $this->templateFactory = $templateFactory;
        $this->systemDir       = $systemDir;
        $this->linkGenerator   = $linkGenerator;
    }

    public function sendByState(TicketOrder $order): ?MailQueue
    {
        $template = $order->getTicketEvent()->findMailTemplate(CMailTicketOrder::class, $order->getState());

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailTicketOrder($order, $this->translator),
            [$order->getEmail()],
            true
        );
    }

    public function sendTickets(TicketOrder $order): ?MailQueue
    {
        $template = $order->getTicketEvent()->findMailTemplate(CMailTicketOrderTickets::class, CMailTicketOrderTickets::IDENTIFIER);

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailTicketOrderTickets($order, $this->createTickets($order)),
            [$order->getEmail()],
            true
        );
    }

    public function sendTicketsBeforeEvent(TicketOrder $order): ?MailQueue
    {
        $template = $order->getTicketEvent()->findMailTemplate(CMailTicketOrderTicketsBeforeEvent::class, CMailTicketOrderTicketsBeforeEvent::IDENTIFIER);

        if ($template === null) {
            return null;
        }

        return $this->mailService->add(
            $template,
            new CMailTicketOrderTicketsBeforeEvent($order, $this->createTickets($order)),
            [$order->getEmail()],
            true
        );
    }

    /**
     * @return array<string>
     */
    public function createTickets(TicketOrder|TicketOrderItem $orderOrItem, bool $download = false): array
    {
        $translator = clone $this->translator;
        $translator->setModule('admin');

        $template = null;
        $footer   = null;

        $template = $this->templateFactory->createTemplate();
        $template->setFile($this->systemDir->getPathApp(['templates', 'pdf', 'pdf-ticket-order-tickets.latte']));
        $template->linkGenerator = $this->linkGenerator;
        $template->orderOrItem   = $orderOrItem;
        
        if ($orderOrItem instanceof TicketOrder) {
            $order       = $orderOrItem;
            $pdfName     = $translator->translate(new SimpleTranslation('ticket-order.pdf.tickets %s', [$order->getOrderNumber()]));
            $pdfFilePath = sprintf('tickets-%s.pdf', $order->getOrderNumber());
        } else {
            $order       = $orderOrItem->getOrder();
            $pdfName     = $translator->translate(new SimpleTranslation('ticket-order.pdf.ticket %s %s', [$order->getOrderNumber(), $orderOrItem->getHash()]));
            $pdfFilePath = sprintf('tickets-%s-%s.pdf', $order->getOrderNumber(), $orderOrItem->getHash());
        }

        if ($template instanceof Template) {
            $mpdfConfig = include $this->systemDir->getPathApp(['templates', 'pdf', '_mpdf-config.php']);

            $pdf = new Mpdf($mpdfConfig['config']);
            $pdf->WriteHTML($template);

            if (isset($mpdfConfig['header']) && $mpdfConfig['header']) {
                $pdf->SetHeader($mpdfConfig['header']);
            }

            if (isset($mpdfConfig['footer']) && $mpdfConfig['footer']) {
                $pdf->SetFooter($mpdfConfig['footer']);
            }

            //$pdf->Output();
            //exit;

            $fileIdentifier = $this->fileStorage->saveContent(
                $pdf->Output('', Destination::STRING_RETURN),
                $pdfFilePath,
                BaseControl::DIR_FILE_ORDER,
                true
            );

            if ($download) {
                $this->fileStorage->download($fileIdentifier, sprintf('%s.pdf', $pdfName));
            }

            return [$fileIdentifier];
        }
    }

}
