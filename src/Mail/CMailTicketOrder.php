<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Mail;

use App\Model\System\Constant;
use Haltuf\Genderer\Genderer;
use DateTimeInterface;
use Nette\Utils\Html;
use ReflectionProperty;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\Ticket\Classes\TicketOrderItemManager;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\Strings;
use function call_user_func;
use function method_exists;
use function sprintf;

class CMailTicketOrder extends CMail
{
    public const TYPE = 'ticket-order';

    private string $name;
    private string $nameReversed;
    private string $nameInflected;
    private string $email;
    private string $phone;

    private string $paymentName;
    private string $paymentContent;

    private string $note;
    private string $createdAt;

    private string $orderNumber;
    private string $priceTotal;
    private string $price;

    public function __construct(TicketOrder $order, Translator $origTranslator)
    {
        $translator = clone $origTranslator;
        $translator->setModule('front');

        $that          = $this;
        $functionMatch = static function (array $m) use ($that): string {
            $match  = trim($m[0], '[]');
            $method = sprintf('get%s', Strings::camelize($match));

            if (method_exists($that, $method)) {
                $value = $that->$method();

                if ($value instanceof DateTimeInterface) {
                    return $value->format('d.m.Y H:i');
                }

                return (string) $value;
            }

            return $match;
        };

        $this->name          = $order->getFullName();
        $this->nameReversed  = $order->getFullName(true);
        $this->nameInflected = (new Genderer())->getVocative($order->getFullName());
        $this->email         = $order->getEmail();
        $this->phone         = $order->getPhone();

        $this->paymentName    = $order->getPayment()->getName();
        $this->paymentContent = $order->getPayment()->getContent();

        $this->note      = $order->getNote();
        $this->createdAt = $order->getCreatedAt()->format('d.m.Y H:i:s');

        $this->orderNumber = $order->getOrderNumber();
        $this->priceTotal  = sprintf('%s Kč', number_format($order->getTotalPrice(), 2, ',', ' '));

        $priceTableRow = static function (string $ticket, int|string $amount, string $price, string $totalPrice, array $attr = []): Html {
            $tr = Html::el('tr', $attr['tr'] ?? []);
            $tr->addHtml(Html::el('td', $attr['td1'] ?? $attr['td'] ?? [])->setHtml($ticket));
            $tr->addHtml(Html::el('td', $attr['td2'] ?? $attr['td'] ?? [])->setHtml($amount));
            $tr->addHtml(Html::el('td', $attr['td3'] ?? $attr['td'] ?? [])->setHtml($price));
            $tr->addHtml(Html::el('td', $attr['td4'] ?? $attr['td'] ?? [])->setHtml($totalPrice));
            return $tr;
        };
        $price         = Html::el('table', ['style' => 'width: 100%; border-collapse: collapse;']);
        $price->addHtml($priceTableRow(
            $translator->translate('mail.ticket-order.price.ticket'),
            $translator->translate('mail.ticket-order.price.amount'),
            $translator->translate('mail.ticket-order.price.price'),
            $translator->translate('mail.ticket-order.price.total-price'),
            [
                'tr'  => ['style' => 'font-weight: bold;'],
                'td1' => ['style' => 'border-bottom: 1px solid black;'],
                'td2' => ['style' => 'text-align: center; border-bottom: 1px solid black;'],
                'td3' => ['style' => 'text-align: right; border-bottom: 1px solid black;'],
                'td4' => ['style' => 'text-align: right; border-bottom: 1px solid black;'],
            ]
        ));

        foreach ($order->getGroupedItems() + $order->getItems([TicketOrderItem::TypePayment])->toArray() as $item) {
            $amount = $item->getAmount();
            if ($item instanceof TicketOrderItemManager) {
                $item = $item->getItem();
            }

            if ($item->getType() === TicketOrderItem::TypeTicket) {
                $type = $item->getTicket()->getTicketType()->getName();
            } else {
                $type = $translator->translate('mail.ticket-order-item.type.payment');
            }

            $price->addHtml($priceTableRow(
                sprintf('%s - %s', $type, $item->getName()),
                $amount,
                sprintf('%s Kč', number_format($item->getPrice(), 2, ',', ' ')),
                sprintf('%s Kč', number_format($item->getTotalPrice(), 2, ',', ' ')),
                [
                    'td2' => ['style' => 'text-align: center;'],
                    'td3' => ['style' => 'text-align: right;'],
                    'td4' => ['style' => 'text-align: right;'],
                ]
            ));
        }

        $price->addHtml($priceTableRow(
            $translator->translate('mail.ticket-order.camp-price-total'),
            '',
            '',
            sprintf('%s Kč', number_format($order->getTotalPrice(), 2, ',', ' ')),
            [
                'tr'  => ['style' => 'font-weight: bold; font-size: 120%;'],
                'td1' => ['style' => 'border-top: 1px solid black;', 'colspan' => '2'],
                'td2' => ['style' => 'display: none;'],
                'td3' => ['style' => 'display: none;'],
                'td4' => ['style' => 'text-align: right; border-top: 1px solid black;', 'colspan' => '2'],
            ]
        ));
        $this->price = (string) $price;

        if (trim($order->getPayment()->getContentEmail()) !== '') {
            $this->paymentContent = Strings::replace($order->getPayment()->getContentEmail(), '~\[[a-z\-]+\]~i', $functionMatch);
        }
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $model = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $description = sprintf('mail.%s.parameter.%s.description', self::TYPE, $property);
            $example     = sprintf('mail.%s.parameter.%s.example', self::TYPE, $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        $mailParameterValue = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $method = sprintf('get%s', Strings::camelize($property));

            if (method_exists($this, $method)) {
                $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
            }
        }

        return $mailParameterValue;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CMailTicketOrder
    {
        $this->name = $name;
        return $this;
    }

    public function getNameReversed(): string
    {
        return $this->nameReversed;
    }

    public function setNameReversed(string $nameReversed): CMailTicketOrder
    {
        $this->nameReversed = $nameReversed;
        return $this;
    }

    public function getNameInflected(): string
    {
        return $this->nameInflected;
    }

    public function setNameInflected(string $nameInflected): CMailTicketOrder
    {
        $this->nameInflected = $nameInflected;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailTicketOrder
    {
        $this->email = $email;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): CMailTicketOrder
    {
        $this->phone = $phone;
        return $this;
    }

    public function getPaymentName(): string
    {
        return $this->paymentName;
    }

    public function setPaymentName(string $paymentName): CMailTicketOrder
    {
        $this->paymentName = $paymentName;
        return $this;
    }

    public function getPaymentContent(): string
    {
        return $this->paymentContent;
    }

    public function setPaymentContent(string $paymentContent): CMailTicketOrder
    {
        $this->paymentContent = $paymentContent;
        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): CMailTicketOrder
    {
        $this->note = $note;
        return $this;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): CMailTicketOrder
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(string $orderNumber): CMailTicketOrder
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    public function getPriceTotal(): string
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(string $priceTotal): CMailTicketOrder
    {
        $this->priceTotal = $priceTotal;
        return $this;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): CMailTicketOrder
    {
        $this->price = $price;
        return $this;
    }


}
