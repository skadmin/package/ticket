<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Mail;

use Haltuf\Genderer\Genderer;
use ReflectionProperty;
use Skadmin\ChildrensCamp\Doctrine\ChildrensCampRegistration\ChildrensCampRegistration;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use SkadminUtils\Utils\Utils\Strings;

class CMailTicketOrderTicketsBeforeEvent extends CMail
{
    public const TYPE       = 'ticker-order-tickets-before-event';
    public const IDENTIFIER = 'ticketsBeforeEvent';

    private string $name;
    private string $nameReversed;
    private string $nameInflected;
    private string $email;
    private string $phone;

    private string $note;

    private string $orderNumber;
    private string $priceTotal;

    private string $attachments;

    /**
     * @param array<string> $attachments
     */
    public function __construct(TicketOrder $order, array $attachments = [])
    {
        $this->name          = $order->getFullName();
        $this->nameReversed  = $order->getFullName(true);
        $this->nameInflected = (new Genderer())->getVocative($order->getFullName());
        $this->email         = $order->getEmail();
        $this->phone         = $order->getPhone();

        $this->note = $order->getNote();

        $this->orderNumber = $order->getOrderNumber();
        $this->priceTotal  = sprintf('%s Kč', number_format($order->getTotalPrice(), 2, ',', ' '));

        $this->attachments = implode(';', $attachments);
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $model = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $description = sprintf('mail.%s.parameter.%s.description', self::TYPE, $property);
            $example     = sprintf('mail.%s.parameter.%s.example', self::TYPE, $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        $mailParameterValue = [];

        /** @var ReflectionProperty $property */
        foreach (self::getProperties(self::class) as $property) {
            $method = sprintf('get%s', Strings::camelize($property));

            if (method_exists($this, $method)) {
                $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
            }
        }

        return $mailParameterValue;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CMailTicketOrderTicketsBeforeEvent
    {
        $this->name = $name;
        return $this;
    }

    public function getNameReversed(): string
    {
        return $this->nameReversed;
    }

    public function setNameReversed(string $nameReversed): CMailTicketOrderTicketsBeforeEvent
    {
        $this->nameReversed = $nameReversed;
        return $this;
    }

    public function getNameInflected(): string
    {
        return $this->nameInflected;
    }

    public function setNameInflected(string $nameInflected): CMailTicketOrderTicketsBeforeEvent
    {
        $this->nameInflected = $nameInflected;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailTicketOrderTicketsBeforeEvent
    {
        $this->email = $email;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): CMailTicketOrderTicketsBeforeEvent
    {
        $this->phone = $phone;
        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): CMailTicketOrderTicketsBeforeEvent
    {
        $this->note = $note;
        return $this;
    }

    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(string $orderNumber): CMailTicketOrderTicketsBeforeEvent
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    public function getPriceTotal(): string
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(string $priceTotal): CMailTicketOrderTicketsBeforeEvent
    {
        $this->priceTotal = $priceTotal;
        return $this;
    }

    public function getAttachments(): string
    {
        return $this->attachments;
    }

    public function setAttachments(string $attachments): CMailTicketOrderTicketsBeforeEvent
    {
        $this->attachments = $attachments;
        return $this;
    }


}
