<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketType;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Entity\Facebook;
use SkadminUtils\DoctrineTraits\Facade;

final class TicketTypeFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TicketType::class;
    }

    public function create(TicketEvent $ticketEvent, string $name, string $content): TicketType
    {
        return $this->update(null, $ticketEvent, $name, $content);
    }

    public function update(?int $id, TicketEvent $ticketEvent, string $name, string $content): TicketType
    {
        $ticketType = $this->get($id);
        $ticketType->update($ticketEvent, $name, $content);

        if (! $ticketType->isLoaded()) {
            $ticketType->setSequence($this->getValidSequence($ticketEvent));
            $ticketType->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($ticketType);
        $this->em->flush();

        return $ticketType;
    }

    public function getValidSequence(TicketEvent $ticketEvent): int
    {
        $sequence = $this->getModel()
            ->select('count(a.id)')
            ->addCriteria(Criteria::create()->where(Criteria::expr()->eq('a.ticketEvent', $ticketEvent)))
            ->getQuery()
            ->getSingleScalarResult();

        return intval($sequence);
    }

    public function get(?int $id = null): TicketType
    {
        if ($id === null) {
            return new TicketType();
        }

        $ticketType = parent::get($id);

        if ($ticketType === null) {
            return new TicketType();
        }

        return $ticketType;
    }

    /**
     * @return TicketType[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    public function getModelForTicketEvent(TicketEvent $ticketEvent): QueryBuilder
    {
        $repository = $this->em
            ->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('ticketEvent', $ticketEvent));

        return $repository->createQueryBuilder('a')
            ->orderBy('a.sequence', 'ASC')
            ->addCriteria($criteria);
    }
}
