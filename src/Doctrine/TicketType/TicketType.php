<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TicketType
{
    use Entity\BaseEntity;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\ManyToOne(targetEntity: TicketEvent::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketEvent $ticketEvent;

    /** @var Collection<int, Ticket> */
    #[ORM\OneToMany(targetEntity: Ticket::class, mappedBy: 'ticketType', indexBy: 'id')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private Collection $tickets;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function update(TicketEvent $ticketEvent, string $name, string $content): void
    {
        $this->ticketEvent = $ticketEvent;
        $this->name        = $name;
        $this->content     = $content;
    }

    public function getTicketEvent(): TicketEvent
    {
        return $this->ticketEvent;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTickets(bool $onlyActive = false): Collection
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        return $this->tickets->matching($criteria);
    }
}
