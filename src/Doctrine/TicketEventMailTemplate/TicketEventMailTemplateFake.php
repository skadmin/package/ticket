<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEventMailTemplate;

use Skadmin\File\Doctrine\File\File;

class TicketEventMailTemplateFake
{

    private string $class;
    private string $identifier;
    private string $subject;
    private string $preheader;
    private string $content;
    /** @var array<string> */
    private array $recipients = [];
    /** @var array<File> */
    private array $attachments = [];

    /**
     * @param array<string> $recipients
     * @param array<File> $attachments
     */
    public function __construct(string $class, $identifier, string $subject, string $preheader, string $content, array $recipients, array $attachments)
    {
        $this->class       = $class;
        $this->identifier  = (string) $identifier;
        $this->subject     = $subject;
        $this->preheader   = $preheader;
        $this->content     = $content;
        $this->recipients  = $recipients;
        $this->attachments = array_filter($attachments, fn($attachment): bool => $attachment instanceof File);
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getPreheader(): string
    {
        return $this->preheader;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string[]
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function getType(): string
    {
        return $this->class::TYPE;
    }

    public function getName(): string
    {
        return sprintf('mail.%s.name', $this->getType());
    }

    public function getParameters(): array
    {
        return $this->class::getModelForSerialize();
    }

    /**
     * @return array<File>
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

}
