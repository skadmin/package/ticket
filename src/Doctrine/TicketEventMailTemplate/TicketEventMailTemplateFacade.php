<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEventMailTemplate;

use Nette\Utils\Arrays;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\File\Doctrine\File\File;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Facade;

final class TicketEventMailTemplateFacade extends Facade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table = TicketEventMailTemplate::class;
    }

    /**
     * @param TicketEvent $ticketEvent
     * @param array<TicketEventMailTemplateFake> $fakeMailTemplates
     * @return TicketEvent
     */
    public function addMailTemplates(TicketEvent $ticketEvent, array $fakeMailTemplates, string $author): TicketEvent
    {
        // odebere nepoužité šablony
        $mailTemplateByClasses = [];
        foreach ($fakeMailTemplates as $fakeMailTemplate) {
            if (! $fakeMailTemplate instanceof TicketEventMailTemplateFake) {
                continue;
            }

            if (! isset($mailTemplateByClasses[$fakeMailTemplate->getClass()])) {
                $mailTemplateByClasses[$fakeMailTemplate->getClass()] = [
                    'type'        => $fakeMailTemplate->getType(),
                    'identifiers' => [],
                ];
            }

            $mailTemplateByClasses[$fakeMailTemplate->getClass()]['identifiers'][] = $fakeMailTemplate->getIdentifier();
        }

        foreach ($mailTemplateByClasses as $class => $mailTemplateByClass) {
            $ticketEvent->removeMailTemplatesExclude($class, $mailTemplateByClass['type'], $mailTemplateByClass['identifiers']);
        }

        // přidáme nové šablony
        foreach ($fakeMailTemplates as $fakeMailTemplate) {
            $mailTemplate = $this->findMailTemplate($ticketEvent, $fakeMailTemplate->getClass(), $fakeMailTemplate->getType(), $fakeMailTemplate->getIdentifier());

            if (! $mailTemplate->isLoaded()) {
                $mailTemplate->create(
                    $ticketEvent,
                    $fakeMailTemplate->getName(),
                    $fakeMailTemplate->getType(),
                    $fakeMailTemplate->getClass(),
                    $fakeMailTemplate->getIdentifier(),
                    $fakeMailTemplate->getParameters()
                );
            }

            $mailTemplate->updateSpecific($fakeMailTemplate->getSubject(), $fakeMailTemplate->getRecipients(), $fakeMailTemplate->getPreheader(), $fakeMailTemplate->getContent(), $fakeMailTemplate->getAttachments(), $author);
            $this->em->persist($mailTemplate);
        }

        $this->em->persist($ticketEvent);
        $this->em->flush();

        return $ticketEvent;
    }

    // e.mail se vytvoří podle stavu, táboru a jedné předlohy... budou se zakládat podle potřeby a ne všechny automaticky

    public function findMailTemplate(TicketEvent $ticketEvent, string $class, string $type, string $identifier): TicketEventMailTemplate
    {
        $mailTemplate = $this->em
            ->getRepository($this->table)
            ->findOneBy([
                'ticketEvent' => $ticketEvent,
                'class'       => $class,
                'type'        => $type,
                'identifier'  => $identifier,
            ]);

        return $mailTemplate instanceof TicketEventMailTemplate ? $mailTemplate : new TicketEventMailTemplate();
    }

    public function addFile(TicketEventMailTemplate $mailTemplate, File $file): void
    {
        $mailTemplate->addFile($file);
        $this->em->flush();
    }

    public function getFileByHash(TicketEventMailTemplate $mailTemplate, string $hash): ?File
    {
        return $mailTemplate->getFileByHash($hash);
    }

    public function removeFileByHash(TicketEventMailTemplate $mailTemplate, string $hash): void
    {
        $mailTemplate->removeFileByHash($hash);
        $this->em->flush();
    }

}
