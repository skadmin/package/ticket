<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEventMailTemplate;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Skadmin\ChildrensTicketEvent\Doctrine\ChildrensTicketEvent\ChildrensTicketEvent;
use Skadmin\File\Doctrine\File\File;
use Skadmin\File\Traits\TFileIdentifier;
use Skadmin\Mailing\Doctrine\Mail\IMailTemplate;
use Skadmin\Mailing\Model\MailTemplateParameter;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TicketEventMailTemplate implements IMailTemplate
{
    use TFileIdentifier;
    use Entity\BaseEntity;
    use Entity\Name;

    #[ORM\Column]
    private string $subject = '';

    #[ORM\Column]
    private string $preheader = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $content = '';

    #[ORM\Column]
    private string $type = ''; // example: ticket-event

    #[ORM\Column]
    private string $identifier = ''; // example: status

    #[ORM\Column]
    private string $class = '';

    /** @var array<string>|bool */
    #[ORM\Column(type: Types::ARRAY)]
    private $recipients = [];

    /** @var mixed[] */
    #[ORM\Column(type: Types::ARRAY)]
    private $parameters = [];

    #[ORM\ManyToOne(targetEntity: TicketEvent::class, inversedBy: 'mailTemplates')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketEvent $ticketEvent;

    /** @var Collection<int, File> */
    #[ORM\ManyToMany(targetEntity: File::class)]
    #[ORM\JoinTable(name: 'ticket_event_mail_template_rel_file')]
    protected Collection $files;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    public function create(TicketEvent $ticketEvent, string $name, string $type, string $class, string $identifier, array $parameters): void
    {
        $this->ticketEvent = $ticketEvent;
        $this->name        = $name;
        $this->type        = $type;
        $this->class       = $class;
        $this->identifier  = $identifier;
        $this->parameters  = $parameters;
    }

    /**
     * @param array<string> $recipients
     * @param array<File> $attachments
     */
    public function updateSpecific(string $subject, array $recipients, string $preheader, string $content, array $attachments, string $lastUpdateAuthor): void
    {
        $this->subject          = $subject;
        $this->recipients       = $recipients;
        $this->preheader        = $preheader;
        $this->content          = $content;
        $this->lastUpdateAuthor = $lastUpdateAuthor;

        foreach ($attachments as $attachment) {
            if ($attachment instanceof File) {
                $this->addFile($attachment);
            }
        }
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return array<string>|string
     */
    public function getRecipients(?string $implodeBy = null)
    {
        $recipients = is_bool($this->recipients) ? [] : $this->recipients;

        if ($implodeBy === null) {
            return $recipients;
        }

        return implode($implodeBy, $recipients);
    }

    public function getPreheader(): string
    {
        return $this->preheader;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return MailTemplateParameter[]
     */
    public function getParameters(): array
    {
        $parameters = [];
        foreach ($this->parameters as $parameter) {
            $name         = $parameter['name'];
            $description  = $parameter['description'];
            $example      = $parameter['example'];
            $parameters[] = new MailTemplateParameter($name, $description, $example);
        }
        return $parameters;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getTicketEvent(): TicketEvent
    {
        return $this->ticketEvent;
    }
}
