<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketOrderItem;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query\AST\OrderByItem;
use Nette\Utils\DateTime;
use Nette\Utils\Random;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use SkadminUtils\DoctrineTraits\Entity;
use SkadminUtils\Utils\Utils\Strings;
use DateTimeInterface;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TicketOrderItem
{
    public const TypeTicket  = 'ticket';
    public const TypePayment = 'payment';

    use Entity\BaseEntity;
    use Entity\Name;
    use Entity\State;
    use Entity\Type;
    use Entity\Hash;

    #[ORM\Column]
    private int $amount = 0;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $appliedAt = null;

    #[ORM\ManyToOne(targetEntity: TicketOrder::class, inversedBy: 'items')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketOrder $order;

    #[ORM\ManyToOne(targetEntity: Ticket::class, inversedBy: 'orderItems')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Ticket $ticket;

    public function create(TicketOrder $order, ?Ticket $ticket, string $type, int $amount, int $price, string $name = ''): void
    {
        $this->state = TicketOrder::StateNew;

        $this->order  = $order;
        $this->ticket = $ticket;

        $this->name   = $ticket instanceof Ticket ? $ticket->getName() : $name;
        $this->type   = $type;
        $this->amount = $amount;
        $this->price  = $price;
    }

    public function update(int $amount, int $price): void
    {
        $this->amount = $amount;
        $this->price  = $price;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getTotalPrice(): int
    {
        return $this->amount * $this->price;
    }

    public function getAppliedAt(): ?DateTimeInterface
    {
        return $this->appliedAt;
    }

    public function isApplied(): bool
    {
        return $this->appliedAt !== null;
    }

    public function canBeApplied(): bool
    {
        return ! $this->isApplied() && $this->state === TicketOrder::StatePaid;
    }

    public function apply(): bool
    {
        if (! $this->canBeApplied()) {
            return false;
        }

        $this->appliedAt = new DateTime();
        return true;
    }

    public function switchApplied(): void
    {
        $this->appliedAt = $this->isApplied() ? null : new DateTime();
    }

    public function getOrder(): TicketOrder
    {
        return $this->order;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setState(int $state): void
    {
        if (! in_array($state, array_keys(TicketOrder::States))) {
            return;
        }

        $this->state = $state;
    }

    #[ORM\PrePersist]
    public function onPreUpdateTicketOrderItem(LifecycleEventArgs $lea): void
    {
        if ($this->getType() !== self::TypeTicket) {
            return;
        }

        $em         = $lea->getEntityManager();
        $repository = $em
            ->getRepository(self::class);

        $existWithHash = static function (string $hash) use ($repository): bool {
            $item = $repository->findOneBy(['hash' => $hash]);

            return $item !== null;
        };

        $createHash = static function (): string {
            $prefix = date('y');
            return strtoupper(sprintf('%s%s-%s-%s',
                trim($prefix) === '' ? date('y') : $prefix,
                Random::generate(max(6 - strlen($prefix), 0)),
                Random::generate(6),
                Random::generate(6)
            ));
        };

        $hash = $createHash();
        while ($existWithHash($hash)) {
            $hash = $createHash();
        }

        $this->hash = $hash;
    }

}
