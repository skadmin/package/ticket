<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketOrderItem;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use SkadminUtils\DoctrineTraits\Facade;

final class TicketOrderItemFacade extends Facade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TicketOrderItem::class;
    }

    public function create(TicketOrder $order, Ticket $ticket, string $type, int $amount, int $price): TicketOrderItem
    {
        $ticketOrderItem = $this->get();
        $ticketOrderItem->create($order, $ticket, $type, $amount, $price);

        $this->em->persist($ticketOrderItem);
        $this->em->flush();

        return $ticketOrderItem;
    }

    public function update(?int $id, int $amount, int $price): TicketOrderItem
    {
        $ticketOrderItem = $this->get($id);
        $ticketOrderItem->update($amount, $price);

        $this->em->persist($ticketOrderItem);
        $this->em->flush();

        return $ticketOrderItem;
    }

    public function updateState(int $id, int $state): TicketOrderItem
    {
        $ticketOrderItem = $this->get($id);
        $ticketOrderItem->setState($state);

        $this->em->persist($ticketOrderItem);
        $this->em->flush();

        return $ticketOrderItem;
    }

    public function switchApplied(int $id): TicketOrderItem
    {
        $ticketOrderItem = $this->get($id);
        $ticketOrderItem->switchApplied();

        $this->em->persist($ticketOrderItem);
        $this->em->flush();

        return $ticketOrderItem;
    }

    public function get(?int $id = null): TicketOrderItem
    {
        if ($id === null) {
            return new TicketOrderItem();
        }

        $ticketOrderItem = parent::get($id);

        if ($ticketOrderItem === null) {
            return new TicketOrderItem();
        }

        return $ticketOrderItem;
    }

    public function remove(int $id): void
    {
        $ticketOrderItem = $this->get($id);

        $this->em->remove($ticketOrderItem);
        $this->em->flush();
    }

    public function findByHash(string $hash): ?TicketOrderItem
    {
        return $this->em
            ->getRepository($this->table)
            ->findOneBy(['hash' => $hash]);
    }
}
