<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEventPayment;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'ticket_event_payment')]
#[ORM\HasLifecycleCallbacks]
class TicketEventPayment
{
    use Entity\BaseEntity;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\ManyToOne(targetEntity: Payment::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Payment $payment;

    #[ORM\ManyToOne(targetEntity: TicketEvent::class, inversedBy: 'payments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketEvent $ticketEvent;

    public function create(TicketEvent $ticketEvent, Payment $payment, int $price): void
    {
        $this->ticketEvent = $ticketEvent;
        $this->payment     = $payment;
        $this->price       = $price;
    }

    public function update(int $price): void
    {
        $this->price = $price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getTicketEvent(): TicketEvent
    {
        return $this->ticketEvent;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }
}
