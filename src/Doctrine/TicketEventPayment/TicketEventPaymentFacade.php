<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEventPayment;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Facade;

final class TicketEventPaymentFacade extends Facade
{

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TicketEventPayment::class;
    }

    public function create(TicketEvent $ticketEvent, Payment $payment, int $price): TicketEventPayment
    {
        $ticketEventPayment = $this->get();
        $ticketEventPayment->create($ticketEvent, $payment, $price);

        $this->em->persist($ticketEventPayment);
        $this->em->flush();

        return $ticketEventPayment;
    }

    public function update(TicketEventPayment $ticketEventPayment, int $price): TicketEventPayment
    {
        $ticketEventPayment->update($price);

        $this->em->persist($ticketEventPayment);
        $this->em->flush();

        return $ticketEventPayment;
    }

    public function remove(TicketEventPayment $ticketEventPayment): void
    {
        $this->em->remove($ticketEventPayment);
        $this->em->flush();
    }

    public function get(?int $id = null): TicketEventPayment
    {
        if ($id === null) {
            return new TicketEventPayment();
        }

        $ticketEventPayment = parent::get($id);

        if ($ticketEventPayment === null) {
            return new TicketEventPayment();
        }

        return $ticketEventPayment;
    }
}
