<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEventPayment;

use Skadmin\Payment\Doctrine\Payment\Payment;

class TicketEventPaymentBox
{
    private int     $price;
    private Payment $payment;

    public function __construct(Payment $payment, int $price)
    {
        $this->payment = $payment;
        $this->price   = $price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }
}
