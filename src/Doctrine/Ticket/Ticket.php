<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\Ticket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use SkadminUtils\DoctrineTraits\Entity;
use SkadminUtils\Utils\Utils\Strings;
use DateTimeInterface;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Ticket
{
    use Entity\BaseEntity;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Perex;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Validity;
    use Entity\Capacity;
    use Entity\Code;

    #[ORM\Column]
    private int $price = 0;

    #[ORM\ManyToOne(targetEntity: TicketEvent::class, inversedBy: 'tickets')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketEvent $ticketEvent;

    #[ORM\ManyToOne(targetEntity: TicketType::class, inversedBy: 'tickets')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketType $ticketType;

    /** @var ArrayCollection<int, TicketOrderItem> */
    #[ORM\OneToMany(targetEntity: TicketOrderItem::class, mappedBy: 'ticket', cascade: ['persist', 'remove'])]
    private ArrayCollection|Collection $orderItems;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
    }

    public function update(
        string $name,
        string $content,
        string $perex,
        bool $isActive,
        int $capacity,
        int $price,
        DateTimeInterface $validityFrom,
        ?DateTimeInterface $validityTo,
        TicketEvent $ticketEvent,
        TicketType $ticketType,
        ?string $imagePreview
    ): void {
        $this->name = $name;
        $this->content = $content;
        $this->perex = trim($perex) === '' ? Strings::truncate($content, 150) : $perex;

        $this->capacity = $capacity;
        $this->price = $price;
        $this->validityFrom = $validityFrom;
        $this->validityTo = $validityTo;

        $this->ticketEvent = $ticketEvent;
        $this->ticketType = $ticketType;

        $this->setIsActive($isActive);

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getTicketEvent(): TicketEvent
    {
        return $this->ticketEvent;
    }

    public function getTicketType(): TicketType
    {
        return $this->ticketType;
    }

    public function isWithoutCapacity(): bool
    {
        return $this->getCapacity() === 0;
    }

    public function getRealCapacity(): int
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->in('state', [TicketOrder::StateNew, TicketOrder::StatePaid]));

        $soldAmount = array_sum(
            $this->orderItems
                ->matching($criteria)
                ->map(fn(TicketOrderItem $toi): int => $toi->getAmount())
                ->toArray()
        );

        return $this->capacity - $soldAmount;
    }

    public function getCountValidTickets(): int
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->in('state', [TicketOrder::StateNew, TicketOrder::StatePaid]));

        return array_sum(
            $this->orderItems
                ->matching($criteria)
                ->map(fn(TicketOrderItem $toi): int => $toi->getAmount())
                ->toArray()
        );
    }

    public function getCountSoldTickets(): int
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->in('state', [TicketOrder::StatePaid]));

        return array_sum(
            $this->orderItems
                ->matching($criteria)
                ->map(fn(TicketOrderItem $toi): int => $toi->getAmount())
                ->toArray()
        );
    }

    public function getCountUsedTickets(): int
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->in('state', [TicketOrder::StatePaid]))
            ->andWhere(Criteria::expr()->neq('appliedAt', null));

        return array_sum(
            $this->orderItems
                ->matching($criteria)
                ->map(fn(TicketOrderItem $toi): int => $toi->getAmount())
                ->toArray()
        );
    }

}
