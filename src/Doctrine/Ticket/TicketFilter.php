<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\Ticket;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\ACriteriaFilter;

use function count;
use function trim;

final class TicketFilter extends ACriteriaFilter
{
    use SmartObject;

    private bool         $onlyValidity;
    private ?TicketEvent $ticketEvent;

    public function __construct(bool $onlyValidity = false, ?TicketEvent $ticketEvent = null)
    {
        $this->onlyValidity = $onlyValidity;
        $this->ticketEvent  = $ticketEvent;
    }

    public static function create(bool $onlyValidity = false, ?TicketEvent $ticketEvent = null): self
    {
        return new self($onlyValidity, $ticketEvent);
    }

    public function isOnlyValidity(): bool
    {
        return $this->onlyValidity;
    }

    public function setOnlyValidity(bool $onlyValidity): void
    {
        $this->onlyValidity = $onlyValidity;
    }

    public function getTicketEvent(): ?TicketEvent
    {
        return $this->ticketEvent;
    }

    public function setTicketEvent(?TicketEvent $ticketEvent): void
    {
        $this->ticketEvent = $ticketEvent;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        if ($this->ticketEvent instanceof TicketEvent) {
            $criteria->andWhere(Criteria::expr()->eq('ticketEvent', $this->getTicketEvent()));
        }

        if (! $this->isOnlyValidity()) {
            return;
        }

        $currentDateTimeStart = (new DateTime())->setTime(0, 0, 0);
        $currentDateTimeEnd   = (new DateTime())->setTime(23, 59, 59);

        $criteria
            ->andWhere(Criteria::expr()->eq('a.isActive', true))
            ->andWhere(Criteria::expr()->orX(
                Criteria::expr()->gte('a.validityTo', $currentDateTimeStart),
                Criteria::expr()->isNull('a.validityTo')
            ))
            ->andWhere(Criteria::expr()->lte('a.validityFrom', $currentDateTimeEnd));
    }
}
