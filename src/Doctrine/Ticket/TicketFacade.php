<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\Ticket;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Ticket\Doctrine\Ticket\TicketFilter;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;

final class TicketFacade extends Facade
{
    use Facade\Code;
    use Facade\Sequence;
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Ticket::class;
    }

    public function create(string $name, string $content, string $perex, bool $isActive, int $capacity, int $price, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo, TicketEvent $ticketEvent, TicketType $ticketType, ?string $imagePreview): Ticket
    {
        return $this->update(null, $name, $content, $perex, $isActive, $capacity, $price, $validityFrom, $validityTo, $ticketEvent, $ticketType, $imagePreview);
    }

    public function update(?int $id, string $name, string $content, string $perex, bool $isActive, int $capacity, int $price, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo, TicketEvent $ticketEvent, TicketType $ticketType, ?string $imagePreview): Ticket
    {
        $ticket = $this->get($id);
        $ticket->update($name, $content, $perex, $isActive, $capacity, $price, $validityFrom, $validityTo, $ticketEvent, $ticketType, $imagePreview);

        if (! $ticket->isLoaded()) {
            $ticket->setCode($this->getValidCode());
            $ticket->setSequence($this->getValidSequence());
            $ticket->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($ticket);
        $this->em->flush();

        return $ticket;
    }

    public function get(?int $id = null): Ticket
    {
        if ($id === null) {
            return new Ticket();
        }

        $ticket = parent::get($id);

        if ($ticket === null) {
            return new Ticket();
        }

        return $ticket;
    }

    /**
     * @return Ticket[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    public function getModelForTicketEvent(TicketEvent $ticketEvent): QueryBuilder
    {
        $repository = $this->em
            ->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('ticketEvent', $ticketEvent));

        return $repository->createQueryBuilder('a')
            ->leftJoin('a.ticketType', 'b')
            ->orderBy('b.sequence', 'ASC')
            ->addOrderBy('a.sequence', 'ASC')
            ->addCriteria($criteria);
    }

    public function findByWebalize(string $webalize): ?Ticket
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function getTicketsCount(?TicketFilter $filter = null): int
    {
        $qb = $this->findTicketsQb($filter);
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return Ticket[]
     */
    public function findTickets(?TicketFilter $filter = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findTicketsQb($filter, $limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findTicketsQb(?TicketFilter $filter = null, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->distinct();
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.sequence', 'ASC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }
}
