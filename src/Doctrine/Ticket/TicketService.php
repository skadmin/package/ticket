<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\Ticket;

use Skadmin\Ticket\Classes\TicketTypeManager;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;

final class TicketService
{
    /**
     * @param array<int, Ticket> $tickets
     * @return array<int, TicketTypeManager>
     */
    public function sortByTicketType(array $tickets): array
    {
        $ticketsByTypes = [];
        foreach ($tickets as $ticket) {
            if (! isset($ticketsByTypes[$ticket->getTicketType()->getSequence()])) {
                $ticketsByTypes[$ticket->getTicketType()->getSequence()] = new TicketTypeManager($ticket->getTicketType());
            }

            $ticketsByTypes[$ticket->getTicketType()->getSequence()]->addTicket($ticket);
        }

        ksort($ticketsByTypes);
        return $ticketsByTypes;
    }
}
