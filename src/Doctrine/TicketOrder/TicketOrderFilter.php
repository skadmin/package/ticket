<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketOrder;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\ACriteriaFilter;

use function count;
use function trim;

final class TicketOrderFilter extends ACriteriaFilter
{
    use SmartObject;

    private TicketEvent $ticketEvent;

    public function __construct(TicketEvent $ticketEvent)
    {
        $this->ticketEvent = $ticketEvent;
    }

    public static function create(TicketEvent $ticketEvent): self
    {
        return new self($ticketEvent);
    }

    public function getTicketEvent(): TicketEvent
    {
        return $this->ticketEvent;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        if (! $this->ticketEvent instanceof TicketEvent) {
            return;
        }

        $criteria->andWhere(Criteria::expr()->eq('ticketEvent', $this->getTicketEvent()));
    }
}
