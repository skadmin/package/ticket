<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketOrder;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Ticket\Classes\TicketOrderItemManager;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use SkadminUtils\DoctrineTraits\Entity;
use SkadminUtils\Utils\Utils\Strings;
use DateTimeInterface;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TicketOrder
{
    public const StateNew      = 1;
    public const StatePaid     = 2;
    public const StateCanceled = 3;

    public const States = [
        self::StateNew      => 'ticket-order.state-new',
        self::StatePaid     => 'ticket-order.state-paid',
        self::StateCanceled => 'ticket-order.state-canceled',
    ];

    use Entity\BaseEntity;
    use Entity\State;
    use Entity\HumanName;
    use Entity\Contact;
    use Entity\Note;
    use Entity\NotePrivate;
    use Entity\OrderNumber;

    private int $totalPrice = 0;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $paidAt = null;

    #[ORM\ManyToOne(targetEntity: TicketEvent::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private TicketEvent $ticketEvent;

    #[ORM\ManyToOne(targetEntity: Payment::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Payment $payment;

    /** @var Collection<int, TicketOrderItem> */
    #[ORM\OneToMany(targetEntity: TicketOrderItem::class, mappedBy: 'order', indexBy: 'ticket', cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function create(TicketEvent $ticketEvent, Payment $payment, string $name, string $surname, string $email, string $phone, string $note, string $notePrivate = ''): void
    {
        $this->state = TicketOrder::StateNew;

        $this->ticketEvent = $ticketEvent;
        $this->payment     = $payment;

        $this->name    = $name;
        $this->surname = $surname;
        $this->email   = $email;
        $this->phone   = $phone;
        $this->note    = $note;

        $this->notePrivate = $notePrivate;
    }

    public function update(int $state, string $name, string $surname, string $phone, string $email, string $note, string $notePrivate): void
    {
        $this->setState($state, true);

        $this->name        = $name;
        $this->surname     = $surname;
        $this->phone       = $phone;
        $this->email       = $email;
        $this->note        = $note;
        $this->notePrivate = $notePrivate;
    }

    public function getTicketEvent(): TicketEvent
    {
        return $this->ticketEvent;
    }

    public function getPayment(): Payment
    {
        return $this->payment;
    }

    public function getTotalPrice(): int
    {
        if ($this->totalPrice > 0) {
            return $this->totalPrice;
        }

        $totalPrice = 0;

        foreach ($this->getItems() as $item) {
            if ($item->getState() == self::StateCanceled) {
                continue;
            }

            $totalPrice += $item->getTotalPrice();
        }

        $this->totalPrice = $totalPrice;

        return $this->totalPrice;
    }

    public function getPaidAt(): ?DateTimeInterface
    {
        return $this->paidAt;
    }

    /**
     * @return Collection<int, TicketOrderItem>
     */
    public function getItems(array $types = []): Collection
    {
        $criteria = Criteria::create();

        if (count($types) > 0) {
            $criteria->andWhere(Criteria::expr()->in('type', $types));
        }

        return $this->items->matching($criteria);
    }

    /**
     * @return array<TicketOrderItemManager>
     */
    public function getGroupedItems(): array
    {
        $groupedItems = [];

        foreach ($this->getItems([TicketOrderItem::TypeTicket]) as $item) {
            if (! isset($groupedItems[$item->getTicket()->getCode()])) {
                $groupedItems[$item->getTicket()->getCode()] = new TicketOrderItemManager($item);
            }

            $groupedItems[$item->getTicket()->getCode()]->addAmount($item->getAmount());
        }

        return $groupedItems;
    }

    public function addItem(TicketOrderItem $item): void
    {
        if ($this->items->contains($item)) {
            return;
        }

        $this->items->add($item);
    }

    public function setState(int $state, bool $changeStateItems): void
    {
        if (! in_array($state, array_keys(self::States))) {
            return;
        }

        $this->state = $state;

        if ($changeStateItems) {
            foreach ($this->getItems() as $item) {
                if (! in_array($item->getState(), [self::StateNew, self::StatePaid])) {
                    continue;
                }

                $item->setState($state);
            }
        }
    }

}
