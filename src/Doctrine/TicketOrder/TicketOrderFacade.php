<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketOrder;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use SkadminUtils\DoctrineTraits\Facade;

final class TicketOrderFacade extends Facade
{

    use Facade\OrderNumber;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = TicketOrder::class;
    }

    public function create(TicketOrder $ticketOrder): TicketOrder
    {
        $ticketOrder->setOrderNumber($this->getValidOrderNumber());

        $this->em->persist($ticketOrder);
        $this->em->flush();

        return $ticketOrder;
    }

    public function update(?int $id, int $state, string $name, string $surname, string $phone, string $email, string $note, string $notePrivate): TicketOrder
    {
        $ticketOrder = $this->get($id);
        $ticketOrder->update($state, $name, $surname, $phone, $email, $note, $notePrivate);

        $this->em->persist($ticketOrder);
        $this->em->flush();

        return $ticketOrder;
    }

    public function updateState(TicketOrder|int $order, int $state, bool $changeStateItems = false): TicketOrder
    {
        if (! $order instanceof TicketOrder) {
            $order = $this->get($order);
        }

        $order->setState($state, $changeStateItems);

        $this->em->persist($order);
        $this->em->flush();

        return $order;
    }

    public function get(?int $id = null): TicketOrder
    {
        if ($id === null) {
            return new TicketOrder();
        }

        $ticketOrder = parent::get($id);

        if ($ticketOrder === null) {
            return new TicketOrder();
        }

        return $ticketOrder;
    }

    public function getModelForTicketEvent(TicketEvent $ticketEvent): QueryBuilder
    {
        $repository = $this->em
            ->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('ticketEvent', $ticketEvent));

        return $repository->createQueryBuilder('a')
            ->leftJoin('a.items', 'ai')
            ->leftJoin('ai.ticket', 'ait')
            ->orderBy('a.orderNumber', 'DESC')
            ->addCriteria($criteria);
    }

    public function getTicketsCount(?TicketOrderFilter $filter = null): int
    {
        $qb = $this->findTicketOrderQb($filter);
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return array<string, string>
     */
    public function getListOfPrivateNotes(): array
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->select('a.notePrivate')
            ->orderBy('a.notePrivate', Criteria::ASC)
            ->distinct();

        $listPrivateNotes = [];
        foreach ($qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY) as $row) {
            $listPrivateNotes[$row['notePrivate']] = $row['notePrivate'];
        }

        return $listPrivateNotes;
    }

    /**
     * @return TicketOrder[]
     */
    public function findTickets(?TicketOrderFilter $filter = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findTicketOrderQb($filter, $limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findTicketOrderQb(?TicketOrderFilter $filter = null, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->distinct();
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.orderNumber', 'DESC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }

    public function findByOrderNumber(string $orderNumber): ?TicketOrder
    {
        return $this->em
            ->getRepository($this->table)
            ->findOneBy(['orderNumber' => $orderNumber]);
    }
}
