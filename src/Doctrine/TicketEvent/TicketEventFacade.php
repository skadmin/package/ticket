<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEvent;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Ticket\Doctrine\TicketEventPayment\TicketEventPayment;
use Skadmin\Ticket\Doctrine\TicketEventPayment\TicketEventPaymentBox;
use Skadmin\Ticket\Doctrine\TicketEventPayment\TicketEventPaymentFacade;
use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;

final class TicketEventFacade extends Facade
{
    use Facade\Webalize;

    private TicketEventPaymentFacade $facadeTicketEventPayment;

    public function __construct(EntityManagerDecorator $em, TicketEventPaymentFacade $facadeTicketEventPayment)
    {
        parent::__construct($em);
        $this->facadeTicketEventPayment = $facadeTicketEventPayment;

        $this->table = TicketEvent::class;
    }

    public function create(string $name, string $content, bool $isActive, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo): TicketEvent
    {
        return $this->update(null, $name, $content, $isActive, $validityFrom, $validityTo);
    }

    public function update(?int $id, string $name, string $content, bool $isActive, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo, bool $changeWebalize = false): TicketEvent
    {
        $ticketEvent = $this->get($id);
        $ticketEvent->update($name, $content, $isActive, $validityFrom, $validityTo);

        if (! $ticketEvent->isLoaded()) {
            $ticketEvent->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $ticketEvent->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($ticketEvent);
        $this->em->flush();

        return $ticketEvent;
    }

    /**
     * @param TicketEventPaymentBox[] $paymentsBox
     */
    public function updatePriceSetting(TicketEvent $ticketEvent, array $paymentsBox): TicketEvent
    {
        // vytvoříme clone všech platebních možností
        $ticketEventPayments = clone $ticketEvent->getPayments();
        foreach ($paymentsBox as $paymentBox) {
            $ticketEventPayment = $ticketEvent->findPayment($paymentBox->getPayment());

            // pokud existuje platební možnost, tak provedeme update a odebereme jí ze seznamu (načetli jsme si nazačátku). Pokud neexistuje, tak jí založíme
            if ($ticketEventPayment instanceof TicketEventPayment) {
                $this->facadeTicketEventPayment->update($ticketEventPayment, $paymentBox->getPrice());
                $ticketEventPayments->removeElement($ticketEventPayment);
            } else {
                $this->facadeTicketEventPayment->create($ticketEvent, $paymentBox->getPayment(), $paymentBox->getPrice());
            }
        }

        // zůstali nám platební možnosti, které již nejsou použity a proto je odebereme
        foreach ($ticketEventPayments as $ticketEventPayment) {
            $this->facadeTicketEventPayment->remove($ticketEventPayment);
        }

        $this->em->persist($ticketEvent);
        $this->em->flush();

        return $ticketEvent;
    }

    public function get(?int $id = null): TicketEvent
    {
        if ($id === null) {
            return new TicketEvent();
        }

        $ticketEvent = parent::get($id);

        if ($ticketEvent === null) {
            return new TicketEvent();
        }

        return $ticketEvent;
    }

    /**
     * @return TicketEvent[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }
}
