<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Doctrine\TicketEvent;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\TicketEventMailTemplate\TicketEventMailTemplate;
use Skadmin\Ticket\Doctrine\TicketEventPayment\TicketEventPayment;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;
use SkadminUtils\Utils\Utils\Strings;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class TicketEvent
{
    use Entity\BaseEntity;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\Validity;

    /** @var Collection<int, Ticket> */
    #[ORM\OneToMany(targetEntity: Ticket::class, mappedBy: 'ticketEvent', indexBy: 'id')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private Collection $tickets;

    /** @var Collection<int, TicketOrder> */
    #[ORM\OneToMany(targetEntity: TicketOrder::class, mappedBy: 'ticketEvent', indexBy: 'id')]
    #[ORM\OrderBy(['orderNumber' => 'DESC'])]
    private Collection $orders;

    /** @var Collection<int, TicketEventPayment> */
    #[ORM\OneToMany(targetEntity: TicketEventPayment::class, mappedBy: 'ticketEvent')]
    private $payments;

    /** @var ArrayCollection<int, TicketEventMailTemplate> */
    #[ORM\OneToMany(targetEntity: TicketEventMailTemplate::class, mappedBy: 'ticketEvent', orphanRemoval: true)]
    private $mailTemplates;

    public function __construct()
    {
        $this->tickets       = new ArrayCollection();
        $this->orders        = new ArrayCollection();
        $this->payments      = new ArrayCollection();
        $this->mailTemplates = new ArrayCollection();
    }

    public function update(string $name, string $content, bool $isActive, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo): void
    {
        $this->name         = $name;
        $this->content      = $content;
        $this->isActive     = $isActive;
        $this->validityFrom = $validityFrom;
        $this->validityTo   = $validityTo;
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTickets(bool $onlyActive = false): Collection
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        return $this->tickets->matching($criteria);
    }

    /**
     * @return Collection<int, TicketOrder>|ArrayCollection
     */
    public function getOrders(array $states = []): Collection
    {
        $criteria = Criteria::create();

        if (count($states) > 0) {
            $criteria->andWhere(Criteria::expr()->in('state', $states));
        }

        return $this->orders->matching($criteria);
    }

    public function findPayment(Payment $payment): ?TicketEventPayment
    {
        $tournamentPayment = $this->payments
            ->matching(Criteria::create()->where(Criteria::expr()->eq('payment', $payment)))
            ->first();

        return $tournamentPayment ? $tournamentPayment : null;
    }

    public function getMinimumPrice(): int
    {
        $minimumPrice = PHP_INT_MAX;

        foreach ($this->getPayments() as $payment) {
            if ($payment->getPrice() >= $minimumPrice) {
                continue;
            }

            $minimumPrice = $payment->getPrice();
        }

        return $minimumPrice === PHP_INT_MAX ? 0 : $minimumPrice;
    }

    /**
     * @return Collection<int, TicketEventPayment>
     */
    public function getPayments()
    {
        $paymentsCollection = new ArrayCollection();

        $payments = [];
        foreach ($this->payments as $payment) {
            $payments[Strings::webalize($payment->getPayment()->getName())] = $payment;
        }

        ksort($payments);

        foreach ($payments as $payment) {
            $paymentsCollection->add($payment);
        }

        return $paymentsCollection;
    }

    /**
     * @return ArrayCollection|TicketEventMailTemplate[]
     */
    public function getMailTemplates()
    {
        return $this->mailTemplates;
    }

    /**
     * @param array<string> $identifiers
     */
    public function removeMailTemplatesExclude(string $class, string $type, array $identifiers): void
    {
        $criteria = Criteria::create();

        $criteria->where(Criteria::expr()->andX(
            Criteria::expr()->eq('class', $class),
            Criteria::expr()->eq('type', $type),
            Criteria::expr()->notIn('identifier', $identifiers),
        ));

        foreach ($this->mailTemplates->matching($criteria) as $mailTemplate) {
            $this->mailTemplates->removeElement($mailTemplate);
        }
    }

    /**
     * @param int|string $identifier
     */
    public function findMailTemplate(string $class, $identifier): ?TicketEventMailTemplate
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->andX(
            Criteria::expr()->eq('class', $class),
            Criteria::expr()->eq('identifier', (string) $identifier)
        ));

        $template = $this->mailTemplates->matching($criteria)->first();
        return is_bool($template) ? null : $template;
    }
}
