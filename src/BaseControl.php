<?php

declare(strict_types=1);

namespace Skadmin\Ticket;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE            = 'ticket';
    public const DIR_IMAGE           = 'ticket';
    public const DIR_FILE_ORDER      = 'ticket-order';
    public const DIR_FILE_ATTACHMENT = 'ticket-attachment';

    public const PRIVILEGE_MANIPULATION = 'manipulation';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-ticket-alt']),
            'items'   => ['overview'],
        ]);
    }
}
