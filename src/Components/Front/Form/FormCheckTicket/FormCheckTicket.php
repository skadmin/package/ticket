<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Front;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Application\Attributes\Persistent;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrderFacade;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItemFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class FormCheckTicket extends FormControl
{
    use APackageControl;

    private TicketOrderItemFacade $facade;

    private ?string $hash;

    /** @var callable[] */
    public array $onProccessTicket = [];

    public function __construct(?string $hash, TicketOrderItemFacade $facade, Translator $translator)
    {
        parent::__construct($translator);
        $this->facade = $facade;

        $this->hash = $hash;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);
        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'form.ticket.front.check-ticket.title';
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formCheckTicket.latte'));

        $template->isSetHash = $this->hash !== null && trim($this->hash) !== '';
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('hashPart1', 'form.ticket.front.check-ticket.hash-part-1')
            ->setRequired('form.ticket.front.check-ticket.hash-part-1.req');
        $form->addText('hashPart2', 'form.ticket.front.check-ticket.hash-part-2')
            ->setRequired('form.ticket.front.check-ticket.hash-part-2.req');
        $form->addText('hashPart3', 'form.ticket.front.check-ticket.hash-part-3')
            ->setRequired('form.ticket.front.check-ticket.hash-part-3.req');

        // BUTTON
        $form->addSubmit('send', 'form.ticket.front.check-ticket.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        $form->setDefaults($this->getDefaults());

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $hash = sprintf('%s-%s-%s', $values->hashPart1, $values->hashPart2, $values->hashPart3);

        $ticket = $this->facade->findByHash($hash);

        $this->onSubmit($form);
        $this->onSuccess($form, $values);
        $this->onProccessTicket($ticket);
        $this->onRedirect();
    }

    protected function getDefaults(): array
    {
        if ($this->hash === null || trim($this->hash) === '') {
            return [];
        }

        [$hashPart1, $hashPart2, $hashPart3] = explode('-', $this->hash);

        return [
            'hashPart1' => $hashPart1,
            'hashPart2' => $hashPart2,
            'hashPart3' => $hashPart3,
        ];
    }

}
