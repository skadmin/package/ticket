<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Front;

use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;

interface IFormCheckTicketFactory
{
    public function create(?string $hash = null): FormCheckTicket;
}
