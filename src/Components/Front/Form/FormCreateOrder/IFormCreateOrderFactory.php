<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Front;

use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;

interface IFormCreateOrderFactory
{
    public function create(TicketEvent $ticketEvent): FormCreateOrder;
}
