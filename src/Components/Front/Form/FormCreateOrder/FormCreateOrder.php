<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Front;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Application\Attributes\Persistent;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrderFacade;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItemFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class FormCreateOrder extends FormControl
{
    use APackageControl;

    private TicketOrderFacade $facade;
    private PaymentFacade     $facadePayment;

    private TicketEvent $ticketEvent;

    private ImageStorage $imageStorage;

    /** @var callable[] */
    public array $onModifyOrder = [];

    public function __construct(TicketEvent $ticketEvent, TicketOrderFacade $facade, PaymentFacade $facadePayment, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade        = $facade;
        $this->facadePayment = $facadePayment;

        $this->ticketEvent = $ticketEvent;

        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);
        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'form.ticket.front.create-order.title';
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formCreateOrder.latte'));

        $template->ticketEvent = $this->ticketEvent;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataPayment = [];
        foreach ($this->ticketEvent->getPayments(true) as $ticketEventPayment) {
            $payment = $ticketEventPayment->getPayment();
            $img     = Html::el('img', [
                'src'   => '/' . $this->imageStorage->fromIdentifier([$payment->getImagePreview(), '50x50', 'exact'])->createLink(),
                'alt'   => $payment->getName(),
                'class' => 'mr-1',
            ]);

            $price = Html::el('span', [
                'class' => 'ml-auto',
            ])->setText($ticketEventPayment->getPrice() === 0 ? 'zdarma' : sprintf('%d Kč', $ticketEventPayment->getPrice()));

            $dataPayment[$payment->getId()] = Html::el('div', [
                'class' => 'd-flex justify-content-end align-items-center',
            ])->addHtml($img)
                ->addText($payment->getName())
                ->addHtml($price);
        }

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.ticket.front.create-order.name')
            ->setRequired('form.ticket.front.create-order.name.req');
        $form->addText('surname', 'form.ticket.front.create-order.surname')
            ->setRequired('form.ticket.front.create-order.surname.req');

        $form->addEmail('email', 'form.ticket.front.create-order.email')
            ->setRequired('form.ticket.front.create-order.email.req');
        $form->addPhone('phone', 'form.ticket.front.create-order.phone')
            ->setRequired('form.ticket.front.create-order.phone.req');

        $form->addTextArea('note', 'form.ticket.front.create-order.note');

        $inputPayment = $form->addRadioList('payment', 'form.ticket.front.create-order.payment', $dataPayment)
            ->setTranslator(null)
            ->setRequired('form.ticket.front.create-order.payment.req')
            ->setDefaultValue(array_keys($dataPayment)[0]);

        foreach (array_keys($dataPayment) as $paymentId) {
            $inputPayment->addCondition(Form::EQUAL, $paymentId)
                ->toggle(sprintf('toggle-payment-%d', $paymentId));
        }

        // CAPTCHA
        $form->addInvisibleReCaptchaInput();

        // MODIFY FORM
        $this->onModifyForm($form);

        // BUTTON
        $form->addSubmit('send', 'form.ticket.front.create-order.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $order = $this->facade->get();

        $data              = clone $values;
        $data->payment     = $this->facadePayment->get($values->payment);
        $data->ticketEvent = $this->ticketEvent;

        $this->onModifyOrder($form, $data, $order);
        $this->onSuccess($form, $values);

        $this->facade->create($order);

        $this->onSendEmail($form, $values, $order);
        $this->onRedraw($form, $values, $order);
        $this->onRedirect($form, $values, $order);
    }

}
