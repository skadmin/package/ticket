<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
