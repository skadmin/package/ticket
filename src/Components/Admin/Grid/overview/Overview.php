<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\Ticket\TicketFacade;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Ticket\Service\TicketOrderService;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private TicketEventFacade $facade;

    private TicketOrderService $serviceTicketOrder;

    public function __construct(TicketEventFacade $facade, Translator $translator, User $user, TicketOrderService $serviceTicketOrder)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
        
        $this->serviceTicketOrder = $serviceTicketOrder;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'ticket.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.validityFrom', 'DESC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.ticket.overview.name')
            ->setRenderer(function (TicketEvent $te): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render' => 'edit',
                        'id' => $te->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href' => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($te->getName());

                return $name;
            });
        $grid->addColumnText('validity', 'grid.ticket.overview.validity')
            ->setRenderer(fn(TicketEvent $te): Html => Html::el('div')->setHtml($te->getValidityFormat('d.m.Y H:i', 'd.m.Y H:i', '%s<br>%s')))
            ->setAlign('center');
        $grid->addColumnText('countOrder', 'grid.ticket.overview.count-order')
            ->setRenderer(fn(TicketEvent $te): int => $te->getOrders([TicketOrder::StateNew, TicketOrder::StatePaid])->count())
            ->setAlign('center');
        $this->addColumnIsActive($grid, 'ticket.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.ticket.overview.name', ['name']);
        $this->addFilterIsActive($grid, 'ticket.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $grid->addAction('overviewTicketOrder', 'grid.ticket.overview.action.overview-ticket-order', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render' => 'overview-ticket-order',
                ])
                ->setIcon('shopping-basket')
                ->setClass('btn btn-xs btn-outline-primary');

            $grid->addAction('overviewTicket', 'grid.ticket.overview.action.overview-ticket', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render' => 'overview-ticket',
                ])
                ->setIcon('ticket-alt')
                ->setClass('btn btn-xs btn-outline-primary');

            $grid->addActionCallback('sendTickets', 'grid.ticket.overview.action.send-tickets', [$this, 'gridOverviewSendTickets'])
                ->setClass('btn btn-xs btn-outline-primary ajax')
                ->setIcon('envelope');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.ticket.overview.action.edit', 'Component:default#1', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render' => 'edit',
                ])
                ->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.ticket.overview.action.new', [
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // OTHER
        $grid->setDefaultFilter(['isActive' => 1]);

        return $grid;
    }

    public function gridOverviewSendTickets(string $id): void
    {
        $event     = $this->facade->get(intval($id));
        $presenter = $this->getPresenterIfExists();

        // zkusíme odeslat e-mail
        foreach ($event->getOrders([TicketOrder::StatePaid]) as $order) {
            $mailQueue = $this->serviceTicketOrder->sendTicketsBeforeEvent($order);
            if ($presenter !== null && $mailQueue !== null) {
                if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                    $message = new SimpleTranslation('grid.ticket.overview.flash.send-tickets.success %s', $order->getEmail());
                    $presenter->flashMessage($message, Flash::SUCCESS);
                } else {
                    $message = new SimpleTranslation('grid.ticket.overview.flash.send-tickets.danger %s', $order->getEmail());
                    $presenter->flashMessage($message, Flash::DANGER);
                }
            }
        }

        $this['grid']->redrawItem($id);
    }
}
