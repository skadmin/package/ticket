<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(int $id): OverviewType;
}
