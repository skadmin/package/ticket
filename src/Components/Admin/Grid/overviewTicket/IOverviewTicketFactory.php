<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

interface IOverviewTicketFactory
{
    public function create(int $id): OverviewTicket;
}
