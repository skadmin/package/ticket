<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\Ticket\TicketFacade;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewTicket extends GridControl
{
    use APackageControl;

    private TicketFacade $facade;
    private TicketTypeFacade $facadeTicketType;

    private LoaderFactory $webLoader;

    private TicketEvent $ticketEvent;

    public function __construct(
        int $id,
        TicketFacade $facade,
        TicketEventFacade $facadeTicketEvent,
        TicketTypeFacade $facadeTicketType,
        Translator $translator,
        User $user,
        LoaderFactory $webLoader
    ) {
        parent::__construct($translator, $user);

        $this->facade = $facade;
        $this->facadeTicketType = $facadeTicketType;

        $this->webLoader = $webLoader;

        $this->ticketEvent = $facadeTicketEvent->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewTicket.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'ticket.overview-ticket.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForTicketEvent($this->ticketEvent));

        // DATA
        $dataTicketTypes = $this->facadeTicketType->getPairs('id', 'name');

        // COLUMNS
        $grid->addColumnText('name', 'grid.ticket.overview-ticket.name')
            ->setRenderer(function (Ticket $t): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render' => 'edit-ticket',
                        'id' => $t->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href' => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($t->getName());

                return $name;
            });
        $grid->addColumnText('validity', 'grid.ticket.overview.validity')
            ->setRenderer(
                fn(Ticket $t): Html => Html::el('div')->setHtml(
                    $t->getValidityFormat('d.m.Y H:i', 'd.m.Y H:i', '%s<br>%s')
                )
            )
            ->setAlign('center');
        $grid->addColumnText('ticketType', 'grid.ticket.overview-ticket.ticket-type', 'ticketType.name')
            ->setAlign('center');
        $grid->addColumnText('capacity', 'grid.ticket.overview-ticket.capacity')
            ->setRenderer(function (Ticket $t): Html {
                return (Html::el('div', ['class' => 'text-nowrap']))
                    ->setText(
                        sprintf(
                            '%s / %s / %s / %s',
                            $t->getCountSoldTickets(),
                            $t->getCountValidTickets(),
                            $t->isWithoutCapacity() ? '-' : $t->getCapacity(),
                            $t->getCountUsedTickets()
                        )
                    );
            })
            ->setAlign('center');
        $grid->addColumnText('price', 'grid.ticket.overview-ticket.price')
            ->setAlign('right');
        $grid->addColumnText('soldAndPrice', 'grid.ticket.overview-ticket.sold-and-price')
            ->setAlign('right')
            ->setRenderer(function (Ticket $t): string {
                return sprintf(
                    '%s / %s,- Kč',
                    $t->getCountSoldTickets(),
                    number_format($t->getPrice() * $t->getCountSoldTickets(), 0, '', ' ')
                );
            });

        // FILTER
        $grid->addFilterText('name', 'grid.ticket.overview-ticket.name');
        $grid->addFilterSelect(
            'ticketType',
            'grid.ticket.overview-ticket.ticket-type',
            Constant::PROMTP_ARR + $dataTicketTypes
        );

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.ticket.overview-ticket.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render' => 'edit-ticket',
                ])
                ->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.ticket.overview-ticket.action.overivew-event', [
            'package' => new BaseControl(),
            'render' => 'overview',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        $grid->addToolbarButton('Component:default#2', 'grid.ticket.overview-ticket.action.overview-type', [
            'package' => new BaseControl(),
            'render' => 'overview-type',
            'id' => $this->ticketEvent->getId(),
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.ticket.overview-ticket.action.new', [
                'package' => new BaseControl(),
                'render' => 'edit-ticket',
                'ticketEventId' => $this->ticketEvent->getId(),
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $ticket = $this->facade->get(intval($itemId));
        $this->facade->sort($itemId, $prevId, $nextId, ['ticket_type_id = %d' => $ticket->getTicketType()->getId()]);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.ticket.overview-ticket.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
