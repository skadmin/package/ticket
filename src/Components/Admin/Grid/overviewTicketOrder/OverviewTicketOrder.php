<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\Ticket\TicketFacade;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrderFacade;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItemFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Ticket\Service\TicketOrderService;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\Column\Column;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\Utils\Utils\PriceFormat;

class OverviewTicketOrder extends GridControl
{
    use APackageControl;

    private TicketOrderFacade     $facade;
    private TicketOrderItemFacade $facadeTicketOrderItem;
    private TicketTypeFacade      $facadeTicketType;
    private TicketFacade          $facadeTicket;

    private TicketEvent $ticketEvent;

    private TicketOrderService $serviceTicketOrder;

    public function __construct(int $id, TicketOrderFacade $facade, TicketOrderItemFacade $facadeTicketOrderItem, TicketEventFacade $facadeTicketEvent, TicketTypeFacade $facadeTicketType, TicketFacade $facadeTicket, TicketOrderService $serviceTicketOrder, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade                = $facade;
        $this->facadeTicketOrderItem = $facadeTicketOrderItem;
        $this->facadeTicketType      = $facadeTicketType;
        $this->facadeTicket          = $facadeTicket;

        $this->serviceTicketOrder = $serviceTicketOrder;

        $this->ticketEvent = $facadeTicketEvent->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewTicketOrder.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'ticket.overview-ticket-order.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $priceFormat = PriceFormat::create();

        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForTicketEvent($this->ticketEvent));

        // DATA
        $dataPrivateNotes = $this->facade->getListOfPrivateNotes();

        $dataStates     = Arrays::map(TicketOrder::States, fn(string $string): string => $this->translator->translate($string));
        $dataStatesBtn  = [
            TicketOrder::StateNew      => 'btn-light',
            TicketOrder::StatePaid     => 'btn-primary',
            TicketOrder::StateCanceled => 'btn-danger',
        ];
        $dataStatesIcon = [
            TicketOrder::StateNew      => 'circle',
            TicketOrder::StatePaid     => 'dollar-sign',
            TicketOrder::StateCanceled => 'times',
        ];

        $dataTicketTypes = [];
        /** @var TicketType $ticketType */
        foreach ($this->facadeTicketType->getModelForTicketEvent($this->ticketEvent)->getQuery()->getResult() as $ticketType) {
            $dataTicketTypes[$ticketType->getId()] = $ticketType->getName();
        }

        $dataTicketNames = [];
        /** @var Ticket $ticket */
        foreach ($this->facadeTicket->getModelForTicketEvent($this->ticketEvent)->orderBy('a.name', 'ASC')->getQuery()->getResult() as $ticket) {
            $dataTicketNames[$ticket->getName()] = $ticket->getName();
        }

        // COLUMNS
        $grid->addColumnText('orderNumber', 'grid.ticket.overview-ticket-order.order-number')
            ->setRenderer(function (TicketOrder $order): Html {
                $render = Html::el();

                $render->addHtml(Html::el('a', [
                    'href'  => $this->getPresenter()->link('Component:default', [
                        'id'      => $order->getId(),
                        'package' => new BaseControl(),
                        'render'  => 'edit-ticket-order',
                    ]),
                    'class' => 'font-weight-bold',
                ])->setText($order->getOrderNumber()));

                if ($order->getNotePrivate()) {
                    $notePrivate = Html::el('small', ['class' => 'text-muted'])
                        ->setText($order->getNotePrivate());

                    $render->addHtml('<br>')
                        ->addHtml($notePrivate);
                }

                return $render;
            });
        $grid->addColumnText('fullName', 'grid.ticket.overview-ticket-order.full-name')
            ->setRenderer(function (TicketOrder $order): Html {
                $render = Html::el();

                $render->addHtml(Html::el('div')
                    ->setText($order->getFullName()));
                $render->addHtml(Html::el('div', ['class' => 'small'])
                    ->addHtml(Html::el('a', [
                        'href' => sprintf('mailto:%s', $order->getEmail()),
                    ])->setText($order->getEmail()))
                );
                $render->addHtml(Html::el('div', ['class' => 'small'])
                    ->addHtml(Html::el('a', [
                        'href' => sprintf('tel:%s', $order->getPhone()),
                    ])->setText($order->getPhone()))
                );

                return $render;
            });
        $grid->addColumnText('totalPrice', 'grid.ticket.overview-ticket-order.total-price')
            ->setAlign(Column::AlignRight)
            ->setRenderer(function (TicketOrder $order) use ($priceFormat): Html {
                $render = Html::el();

                $render->addHtml(Html::el('div')
                    ->setText($priceFormat->toString($order->getTotalPrice())));

                return $render;
            });

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $griColumnStatus = $grid->addColumnStatus('state', 'grid.ticket.overview-ticket-order.state')
                ->setAlign('center');
            foreach ($dataStates as $stateId => $state) {
                $griColumnStatus
                    ->addOption($stateId, $state)
                    ->setIcon($dataStatesIcon[$stateId])
                    ->setClass($dataStatesBtn[$stateId])
                    ->endOption();
            }
            $griColumnStatus->onChange[] = [$this, 'overviewTicketOrderOnChangeStatus'];
        } else {
            $grid->addColumnText('state', 'grid.ticket.overview-ticket-order.state')
                ->setReplacement($dataStates)
                ->setAlign('center');
        }

        //$grid->addColumnText('tags', 'grid.ticket.overview-ticket-order.tags')
        //    ->setAlign('center')
        //    ->setRenderer(function (TicketOrder $order): Html {
        //        $render = Html::el();
        //
        //        if ($order->getPaidAt() instanceof \DateTimeInterface) {
        //            $render->addHtml(Html::el('span', [
        //                'title'          => $this->translator->translate(new SimpleTranslation('grid.ticket.overview-ticket-order.state.paid-at %s', [$order->getPaidAt()->format('d.m.Y H:i:s')])),
        //                'data-toggle'    => 'tooltip',
        //                'data-placement' => 'right',
        //            ])->addHtml(Html::el('i', ['class' => 'text-primary fas fa-fw fa-dollar-sign mx-1'])));
        //        }
        //
        //        return $render;
        //    });

        $grid->addColumnText('items', 'grid.ticket.overview-ticket-order.items')
            ->setRenderer(function (TicketOrder $order) use ($priceFormat, $dataStates): Html {
                $render = Html::el();

                foreach ($order->getItems([TicketOrderItem::TypeTicket]) as $item) {
                    $ticketPrice    = $priceFormat->toString($item->getPrice());
                    $ticketTypeName = $item->getTicket()->getTicketType()->getName();
                    $ticketName     = $item->getTicket()->getName();

                    $icons = Html::el();

                    $tagStateTitle = $dataStates[$item->getState()];
                    switch ($item->getState()) {
                        case TicketOrder::StatePaid:
                            $icons->addHtml(Html::el('i', [
                                'class'          => 'text-primary fas fa-fw fa-check mx-1',
                                'title'          => $tagStateTitle,
                                'data-toggle'    => 'tooltip',
                                'data-placement' => 'right',
                            ]));
                            break;
                        case TicketOrder::StateCanceled:
                            $icons->addHtml(Html::el('i', [
                                'class'          => 'text-danger fas fa-fw fa-times mx-1',
                                'title'          => $tagStateTitle,
                                'data-toggle'    => 'tooltip',
                                'data-placement' => 'right',
                            ]));
                            break;
                        default:
                            $icons->addHtml(Html::el('i', [
                                'class'          => 'text-gray fas fa-fw fa-question mx-1',
                                'title'          => $tagStateTitle,
                                'data-toggle'    => 'tooltip',
                                'data-placement' => 'right',
                            ]));
                            break;
                    }

                    $iconDownload = Html::el('a', [
                        'href'           => $this->link('DownloadTicketOrderItemPdfTicket!', ['id' => $item->getId()]),
                        'title'          => $this->translator->translate('grid.ticket.overview-ticket-order.download'),
                        'data-toggle'    => 'tooltip',
                        'data-placement' => 'right',
                    ])->addHtml(Html::el('i', ['class' => 'fas fa-fw fa-download mx-1']));
                    $icons->addHtml($iconDownload);

                    if ($order->getState() === TicketOrder::StatePaid && $item->getState() !== TicketOrder::StateCanceled) {
                        if ($item->isApplied()) {
                            $tagAppliedTitle     = $this->translator->translate(new SimpleTranslation('grid.ticket.overview-ticket-order.applied-at %s', [$item->getAppliedAt()->format('d.m.Y H:i:s')]));
                            $tagAppliedIconClass = 'text-danger';
                        } else {
                            $tagAppliedTitle     = $this->translator->translate('grid.ticket.overview-ticket-order.not-applied');
                            $tagAppliedIconClass = 'text-primary';
                        }

                        $iconApplied = Html::el('a', [
                            'href'           => $this->link('changeOrderItemApplied!', ['id' => $item->getId()]),
                            'class'          => 'ajax',
                            'title'          => $tagAppliedTitle,
                            'data-toggle'    => 'tooltip',
                            'data-placement' => 'right',
                        ])->addHtml(Html::el('i', ['class' => sprintf('%s fas fa-fw fa-qrcode mx-1', $tagAppliedIconClass)]));
                        $icons->addHtml($iconApplied);
                    }

                    $render->addHtml(
                        Html::el('div', ['class' => 'text-nowrap mb-2'])
                            ->setHtml(sprintf('%s%dx %s - %s (%s)<code class="small d-block text-primary font-weight-bold">%s</code>',
                                $icons,
                                $item->getAmount(),
                                $ticketTypeName,
                                $ticketName,
                                $ticketPrice,
                                $item->getHash()
                            ))
                    );
                }

                return $render;
            });

        // FILTER
        $grid->setOuterFilterRendering();
        $grid->setOuterFilterColumnsCount(3);
        $grid->addFilterText('orderNumber', 'grid.ticket.overview-ticket-order.order-number');
        $grid->addFilterText('fullName', 'grid.ticket.overview-ticket-order.full-name', ['name', 'surname', 'email', 'phone']);
        $grid->addFilterSelect('state', 'grid.ticket.overview-ticket-order.state', Constant::PROMTP_ARR + $dataStates);
        $grid->addFilterText('itemHash', 'grid.ticket.overview-ticket-order.item-hash', 'ai.hash');
        $grid->addFilterSelect('itemTicketType', 'grid.ticket.overview-ticket-order.item-ticket-type', Constant::PROMTP_ARR + $dataTicketTypes, 'ait.ticketType');
        $grid->addFilterSelect('itemTicketName', 'grid.ticket.overview-ticket-order.item-ticket-name', Constant::PROMTP_ARR + $dataTicketNames, 'ait.name');
        $grid->addFilterSelect('notePrivate', 'grid.ticket.overview-ticket-order.note-private', Constant::PROMTP_ARR + $dataPrivateNotes);

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addActionCallback('sendTickets', 'grid.ticket.overview-ticket-order.action.send-tickets', [$this, 'gridOverviewTicketOrderSendTickets'])
                ->setClass('btn btn-xs btn-outline-primary ajax')
                ->setIcon('envelope');

            $grid->addActionCallback('pdfTickets', 'grid.ticket.overview-ticket-order.action.pdf-tickets', [$this, 'gridOverviewTicketOrderPdfTickets'])
                ->setClass('btn btn-xs btn-outline-primary')
                ->setIcon('download');

            $grid->addAction('edit', 'grid.ticket.overview-ticket-order.action.edit', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'edit-ticket-order',
                ])
                ->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.ticket.overview-ticket-order.action.new', [
                'package'       => new BaseControl(),
                'render'        => 'edit-ticket-order',
                'ticketEventId' => $this->ticketEvent->getId(),
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-primary');
        }

        return $grid;
    }

    public function handleChangeOrderItemApplied(string $id): void
    {
        $id = intval($id);

        $orderItem = $this->facadeTicketOrderItem->switchApplied($id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $appliedState = $this->translator->translate(sprintf('applied-%s', $orderItem->isApplied() ? 'yes' : 'no'));
            $message      = new SimpleTranslation('grid.ticket.overview-ticket-order.flash.change-item-applied %s', [$appliedState]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        $this['grid']->redrawItem($orderItem->getOrder()->getId());
    }

    public function handleDownloadTicketOrderItemPdfTicket(string $id): void
    {
        $this->serviceTicketOrder->createTickets($this->facadeTicketOrderItem->get(intval($id)), true);
    }

    public function overviewTicketOrderOnChangeStatus(string $id, string $newState): void
    {
        $id       = intval($id);
        $newState = intval($newState);

        $order = $this->facade->updateState($id, $newState, true);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $newStatusText = $this->translator->translate(TicketOrder::States[$order->getState()]);
            $message       = new SimpleTranslation('grid.ticket.overview-ticket-order.flash.change-state %s %s', [$order->getOrderNumber(), $newStatusText]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        // zkusíme odeslat e-mail
        $mailQueue = $this->serviceTicketOrder->sendByState($order);
        if ($presenter !== null && $mailQueue !== null) {
            if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $message = new SimpleTranslation('grid.ticket.overview-ticket-order.flash.change-state.success %s', $order->getEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
            } else {
                $message = new SimpleTranslation('grid.ticket.overview-ticket-order.flash.change-state.danger %s', $order->getEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        if ($order->getState() === TicketOrder::StatePaid) {
            $this->serviceTicketOrder->sendTickets($order);
        }

        $this['grid']->redrawItem($order->getId());
    }

    public function gridOverviewTicketOrderPdfTickets(string $id): void
    {
        $this->serviceTicketOrder->createTickets($this->facade->get(intval($id)), true);
    }

    public function gridOverviewTicketOrderSendTickets(string $id): void
    {
        $order     = $this->facade->get(intval($id));
        $presenter = $this->getPresenterIfExists();

        // zkusíme odeslat e-mail
        $mailQueue = $this->serviceTicketOrder->sendTickets($order);
        if ($presenter !== null && $mailQueue !== null) {
            if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                $message = new SimpleTranslation('grid.ticket.overview-ticket-order.flash.send-tickets.success %s', $order->getEmail());
                $presenter->flashMessage($message, Flash::SUCCESS);
            } else {
                $message = new SimpleTranslation('grid.ticket.overview-ticket-order.flash.send-tickets.danger %s', $order->getEmail());
                $presenter->flashMessage($message, Flash::DANGER);
            }
        }

        $this['grid']->redrawItem($id);
    }
}
