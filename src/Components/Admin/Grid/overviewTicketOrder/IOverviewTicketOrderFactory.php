<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

interface IOverviewTicketOrderFactory
{
    public function create(int $id): OverviewTicketOrder;
}
