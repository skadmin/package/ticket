<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\Ticket\TicketFacade;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditTicket extends FormWithUserControl
{
    use APackageControl;

    private TicketFacade      $facade;
    private TicketEventFacade $facadeTicketEvent;
    private TicketTypeFacade  $facadeTicketType;

    private LoaderFactory $webLoader;
    private ImageStorage  $imageStorage;

    private Ticket $ticket;
    private ?int   $ticketEventId = null;

    public function __construct(?int $id, Request $request, TicketFacade $facade, TicketEventFacade $facadeTicketEvent, TicketTypeFacade $facadeTicketType, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade            = $facade;
        $this->facadeTicketEvent = $facadeTicketEvent;
        $this->facadeTicketType  = $facadeTicketType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->ticket = $this->facade->get($id);

        if ($this->ticket->isLoaded()) {
            $this->ticketEventId = $this->ticket->getTicketEvent()->getId();
        } else {
            $this->ticketEventId = intval($request->getQuery('ticketEventId'));

            if ($this->ticketEventId === 0) {
                $this->ticketEventId = intval($request->getPost('ticketEventId'));
            }
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->ticket->isLoaded()) {
            return new SimpleTranslation('ticket.edit-ticket.title - %s', $this->ticket->getName());
        }

        return 'ticket.edit-ticket.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editTicket.latte');

        $template->ticket = $this->ticket;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        \Tracy\Debugger::barDump($this->ticketEventId);

        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $ticketEvent = $this->facadeTicketEvent->get($this->ticketEventId);

        $dataTicketType = [];
        /** @var TicketType $ticketType */
        foreach ($this->facadeTicketType->getModelForTicketEvent($ticketEvent)->getQuery()->getResult() as $ticketType) {
            $dataTicketType[$ticketType->getId()] = $ticketType->getName();
        }

        // INPUT
        if (! $this->ticket->isLoaded()) {
            $form->addHidden('ticketEventId', $this->ticketEventId);
        }

        $form->addText('name', 'form.ticket.edit-ticket.name')
            ->setRequired('form.ticket.edit-ticket.name.req');
        $form->addTextArea('perex', 'form.ticket.edit-ticket.perex');
        $form->addTextArea('content', 'form.ticket.edit-ticket.content');

        $form->addInteger('price', 'form.ticket.edit-ticket.price')
            ->setRequired('form.ticket.edit-ticket.price.req')
            ->setHtmlAttribute('min', 0);
        $form->addInteger('capacity', 'form.ticket.edit-ticket.capacity')
            ->setHtmlAttribute('min', 0)
            ->setDefaultValue(0);

        $form->addText('validity', 'form.ticket.edit-ticket.validity')
            ->setRequired('form.ticket.edit-ticket.validity.req')
            ->setHtmlAttribute('data-daterange', 'DD.MM.YYYY HH:mm')
            ->setHtmlAttribute('data-daterange-timepicker');

        $form->addImageWithRFM('imagePreview', 'form.ticket.edit-ticket.image-preview');

        $form->addCheckbox('isActive', 'form.ticket.edit-ticket.is-active')
            ->setDefaultValue(true);

        $form->addSelect('ticketType', 'form.ticket.edit-ticket.ticket-type', $dataTicketType)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.ticket.edit-ticket.ticket-type.req')
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.ticket.edit-ticket.send');
        $form->addSubmit('sendBack', 'form.ticket.edit-ticket.send-back');
        $form->addSubmit('back', 'form.ticket.edit-ticket.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        [$validityFrom, $validityTo] = Arrays::map(explode(' - ', $values->validity), static function (string $date): DateTime {
            $date = DateTime::createFromFormat('d.m.Y H:i', $date);

            return is_bool($date) ? new DateTime() : $date;
        });
        assert($validityFrom instanceof DateTime);
        assert($validityTo instanceof DateTime);

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->ticket->isLoaded()) {
            if ($identifier !== null && $this->ticket->getImagePreview() !== null) {
                $this->imageStorage->delete($this->ticket->getImagePreview());
            }

            $ticket = $this->facade->update(
                $this->ticket->getId(),
                $values->name,
                $values->content,
                $values->perex,
                $values->isActive,
                $values->capacity,
                $values->price,
                $validityFrom,
                $validityTo,
                $this->ticket->getTicketEvent(),
                $this->facadeTicketType->get($values->ticketType),
                $identifier
            );
            $this->onFlashmessage('form.ticket.edit-ticket.flash.success.update', Flash::SUCCESS);
        } else {
            $ticket = $this->facade->create(
                $values->name,
                $values->content,
                $values->perex,
                $values->isActive,
                $values->capacity,
                $values->price,
                $validityFrom,
                $validityTo,
                $this->facadeTicketEvent->get(intval($values->ticketEventId)),
                $this->facadeTicketType->get($values->ticketType),
                $identifier
            );
            $this->onFlashmessage('form.ticket.edit-ticket.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-ticket',
            'id'      => $ticket->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-ticket',
            'id'      => $this->ticketEventId,
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->ticket->isLoaded()) {
            return [];
        }

        return [
            'name'       => $this->ticket->getName(),
            'perex'      => $this->ticket->getPerex(),
            'content'    => $this->ticket->getContent(),
            'price'      => $this->ticket->getPrice(),
            'capacity'   => $this->ticket->getCapacity(),
            'isActive'   => $this->ticket->isActive(),
            'ticketType' => $this->ticket->getTicketType()->getId(),
            'validity'   => $this->ticket->getValidityFormat('d.m.Y H:i', 'd.m.Y H:i'),
        ];
    }
}
