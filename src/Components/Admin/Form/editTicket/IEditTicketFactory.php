<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

interface IEditTicketFactory
{
    public function create(?int $id = null): EditTicket;
}
