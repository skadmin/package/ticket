<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Contributte\FormMultiplier\Submitter;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;
use Nette\Forms\Form as NetteForm;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Nette\Utils\Validators;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\Ticket\TicketFacade;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEvent;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrder;
use Skadmin\Ticket\Doctrine\TicketOrder\TicketOrderFacade;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItemFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Ticket\Service\TicketOrderService;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use SkadminUtils\Utils\Utils\PriceFormat;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditTicketOrder extends FormWithUserControl
{
    use APackageControl;

    private TicketOrderFacade     $facade;
    private TicketOrderItemFacade $facadeTicketOrderItem;
    private TicketFacade          $facadeTicket;
    private TicketEventFacade     $facadeTicketEvent;
    private PaymentFacade         $facadePayment;

    private TicketOrderService $serviceTicketOrder;

    private LoaderFactory $webLoader;

    private TicketOrderItem $ticketOrderItem;
    private TicketOrder     $ticketOrder;
    private ?TicketEvent    $ticketEvent = null;

    public function __construct(?int $id, TicketOrderFacade $facade, TicketOrderItemFacade $facadeTicketOrderItem, TicketFacade $facadeTicket, TicketEventFacade $facadeTicketEvent, PaymentFacade $facadePayment, TicketOrderService $serviceTicketOrder, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade                = $facade;
        $this->facadeTicketOrderItem = $facadeTicketOrderItem;
        $this->facadeTicket          = $facadeTicket;
        $this->facadeTicketEvent     = $facadeTicketEvent;
        $this->facadePayment         = $facadePayment;

        $this->serviceTicketOrder = $serviceTicketOrder;

        $this->webLoader = $webLoader;

        $this->ticketOrder = $this->facade->get($id);

        if ($this->ticketOrder->isLoaded()) {
            $this->ticketEvent = $this->ticketOrder->getTicketEvent();
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        if ($this->ticketEvent === null && isset($_REQUEST['ticketEventId'])) {
            $this->ticketEvent = $this->facadeTicketEvent->get(intval($_REQUEST['ticketEventId']));
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->ticketOrder->isLoaded()) {
            return new SimpleTranslation('ticket.edit-ticket-order.title - %s', $this->ticketOrder->getOrderNumber());
        }

        return 'ticket.edit-ticket-order.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editTicketOrder.latte');

        $template->ticketOrder = $this->ticketOrder;
        $template->_form       = $this['form'];

        $dataTicketPrices = [];
        /** @var Ticket $ticket */
        $priceFormat = PriceFormat::create();
        foreach ($this->facadeTicket->getModelForTicketEvent($this->ticketEvent)->getQuery()->getResult() as $ticket) {
            $dataTicketPrices[$ticket->getId()] = $priceFormat->toString($ticket->getPrice());
        }
        $template->ticketPrices = $dataTicketPrices;

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataTickets = [];
        /** @var Ticket $ticket */
        foreach ($this->facadeTicket->getModelForTicketEvent($this->ticketEvent)->getQuery()->getResult() as $ticket) {
            $dataTickets[$ticket->getId()] = sprintf('%s - %s', $ticket->getTicketType()->getName(), $ticket->getName());
        }

        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addHidden('ticketEventId', $this->ticketEvent->getId());

        $form->addText('name', 'form.ticket.edit-ticket-order.name')
            ->setRequired('form.ticket.edit-ticket-order.name.req');
        $form->addText('surname', 'form.ticket.edit-ticket-order.surname')
            ->setRequired('form.ticket.edit-ticket-order.surname.req');

        $form->addPhone('phone', 'form.ticket.edit-ticket-order.phone')
            ->setRequired('form.ticket.edit-ticket-order.phone.req');
        $form->addEmail('email', 'form.ticket.edit-ticket-order.email')
            ->setRequired('form.ticket.edit-ticket-order.email.req');

        $form->addTextArea('note', 'form.ticket.edit-ticket-order.note');
        $form->addTextArea('notePrivate', 'form.ticket.edit-ticket-order.note-private');

        $form->addSelect('state', 'form.ticket.edit-ticket-order.state', TicketOrder::States);

        $form->addCheckbox('sendEmailByState', 'form.ticket.edit-ticket-order.send-email-by-state');
        $form->addCheckbox('sendEmailWithTickets', 'form.ticket.edit-ticket-order.send-email-with-tickets');

        // FORM ITEMS
        $formItems = $form->addMultiplier('items', function (Container $container, NetteForm $form) use ($dataTickets) {
            $container->addHidden('id');
            $container->addHidden('hash');
            $container->addSelect('state', 'form.ticket.edit-ticket-order.items.state', TicketOrder::States);
            $container->addSelect('ticket', 'form.ticket.edit-ticket-order.items.ticket', Constant::PROMTP_ARR + $dataTickets)
                ->setTranslator(null);
        });

        $formItems->addCreateButton('form.ticket.edit-ticket-order.items.add')
            ->setNoValidate()
            ->addOnCreateCallback(function (Submitter $submitter) {
                $submitter->onClick[] = function (): void {
                    $this->redrawControl('snipAreaForm');
                    $this->redrawControl('snipFormItems');
                };
            });
        $formItems->addRemoveButton('form.ticket.edit-ticket-order.items.remove')
            ->addOnCreateCallback(function (SubmitButton $submitter) {
                $submitter->onClick[] = function (): void {
                    $this->redrawControl('snipAreaForm');
                    $this->redrawControl('snipFormItems');
                };
            });

        // BUTTON
        $form->addSubmit('send', 'form.ticket.edit-ticket-order.send');
        $form->addSubmit('sendBack', 'form.ticket.edit-ticket-order.send-back');
        $form->addSubmit('back', 'form.ticket.edit-ticket-order.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->ticketOrder->isLoaded()) {
            $ticketOrder = $this->facade->update(
                $this->ticketOrder->getId(),
                $values->state,
                $values->name,
                $values->surname,
                $values->phone,
                $values->email,
                $values->note,
                $values->notePrivate
            );
            $this->onFlashmessage('form.ticket.edit-ticket-order.flash.success.update', Flash::SUCCESS);
        } else {
            $newTicketOrder = new TicketOrder();
            $newTicketOrder->create(
                $this->ticketEvent,
                $this->facadePayment->getAllForUnpaid()[0],
                $values->name,
                $values->surname,
                $values->email,
                $values->phone,
                $values->note,
                $values->notePrivate
            );
            $ticketOrder = $this->facade->create($newTicketOrder);

            $this->onFlashmessage('form.ticket.edit-ticket-order.flash.success.create', Flash::SUCCESS);
        }

        $currentItems = [];
        foreach ($ticketOrder->getItems([TicketOrderItem::TypeTicket]) as $ticket) {
            $currentItems[$ticket->getId()] = $ticket;
        }

        foreach ($values->items as $item) {
            $itemId = intval($item->id);

            if ($itemId !== 0) {
                $this->facadeTicketOrderItem->updateState($itemId, $item->state);
                unset($currentItems[$itemId]);
            } elseif (Validators::isNumericInt($item->ticket)) {
                $ticket = $this->facadeTicket->get($item->ticket);
                $this->facadeTicketOrderItem->create(
                    $ticketOrder,
                    $ticket,
                    TicketOrderItem::TypeTicket,
                    1,
                    $ticket->getPrice()
                );
            }
        }

        foreach ($currentItems as $currentItem) {
            $this->facadeTicketOrderItem->remove($currentItem->getId());
        }

        if ($values->sendEmailByState) {
            // zkusíme odeslat e-mail
            $mailQueue = $this->serviceTicketOrder->sendByState($ticketOrder);
            if ($mailQueue !== null) {
                if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                    $this->onFlashmessage(new SimpleTranslation('form.ticket.edit-ticket-order.flash.send-by-state.success %s', $ticketOrder->getEmail()), Flash::SUCCESS);
                } else {
                    $this->onFlashmessage(new SimpleTranslation('form.ticket.edit-ticket-order.flash.send-by-state.danger %s', $ticketOrder->getEmail()), Flash::DANGER);
                }
            }
        }

        if ($values->sendEmailWithTickets) {
            // zkusíme odeslat e-mail
            $mailQueue = $this->serviceTicketOrder->sendTickets($ticketOrder);
            if ($mailQueue !== null) {
                if ($mailQueue->getStatus() === MailQueue::STATUS_SENT) {
                    $this->onFlashmessage(new SimpleTranslation('form.ticket.edit-ticket-order.flash.send-tickets.success %s', $ticketOrder->getEmail()), Flash::SUCCESS);
                } else {
                    $this->onFlashmessage(new SimpleTranslation('form.ticket.edit-ticket-order.flash.send-tickets.danger %s', $ticketOrder->getEmail()), Flash::DANGER);
                }
            }
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-ticket-order',
            'id'      => $ticketOrder->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-ticket-order',
            'id'      => $this->ticketOrder->getTicketEvent()->getId(),
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->ticketOrder->isLoaded()) {
            return [];
        }

        $items = $this->ticketOrder->getItems([TicketOrderItem::TypeTicket])
            ->map(fn(TicketOrderItem $toi): array => [
                'id'     => $toi->getId(),
                'hash'   => $toi->getHash(),
                'state'  => $toi->getState(),
                'ticket' => $toi->getTicket()->getId(),
            ])
            ->toArray();

        return [
            'name'        => $this->ticketOrder->getName(),
            'surname'     => $this->ticketOrder->getSurname(),
            'email'       => $this->ticketOrder->getEmail(),
            'phone'       => $this->ticketOrder->getPhone(),
            'note'        => $this->ticketOrder->getNote(),
            'notePrivate' => $this->ticketOrder->getNotePrivate(),
            'state'       => $this->ticketOrder->getState(),
            'items'       => $items,
        ];
    }
}
