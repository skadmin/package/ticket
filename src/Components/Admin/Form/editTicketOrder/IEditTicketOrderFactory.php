<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

interface IEditTicketOrderFactory
{
    public function create(?int $id = null): EditTicketOrder;
}
