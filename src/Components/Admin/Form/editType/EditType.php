<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Application\Attributes\Persistent;
use Nette\ComponentModel\IContainer;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ticket\BaseControl;
use Skadmin\Ticket\Doctrine\TicketEvent\TicketEventFacade;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;
use Skadmin\Ticket\Doctrine\TicketType\TicketTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private TicketTypeFacade  $facade;
    private TicketEventFacade $facadeTicketEvent;
    private TicketType        $ticketType;

    private LoaderFactory $webLoader;

    private ?int $ticketEventId = null;

    public function __construct(?int $id, Request $request, TicketTypeFacade $facade, TicketEventFacade $facadeTicketEvent, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade            = $facade;
        $this->facadeTicketEvent = $facadeTicketEvent;

        $this->webLoader = $webLoader;

        $this->ticketType = $this->facade->get($id);

        if ($this->ticketType->isLoaded()) {
            $this->ticketEventId = $this->ticketType->getTicketEvent()->getId();
        } else {
            $this->ticketEventId = intval($request->getQuery('ticketEventId'));

            if ($this->ticketEventId === 0) {
                $this->ticketEventId = intval($request->getPost('ticketEventId'));
            }
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->ticketType->isLoaded()) {
            return new SimpleTranslation('ticket-type.edit.title - %s', $this->ticketType->getName());
        }

        return 'ticket-type.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        if (! $this->ticketType->isLoaded()) {
            $form->addHidden('ticketEventId', $this->ticketEventId);
        }

        $form->addText('name', 'form.ticket-type.edit.name')
            ->setRequired('form.ticket-type.edit.name.req');
        $form->addTextArea('content', 'form.ticket-type.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.ticket-type.edit.send');
        $form->addSubmit('sendBack', 'form.ticket-type.edit.send-back');
        $form->addSubmit('back', 'form.ticket-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->ticketType->isLoaded()) {
            $ticketType = $this->facade->update(
                $this->ticketType->getId(),
                $this->ticketType->getTicketEvent(),
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.ticket-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $ticketType = $this->facade->create(
                $this->facadeTicketEvent->get(intval($values->ticketEventId)),
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.ticket-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $ticketType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
            'id'      => $this->ticketEventId,
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->ticketType->isLoaded()) {
            return [];
        }

        return [
            'name'    => $this->ticketType->getName(),
            'content' => $this->ticketType->getContent(),
        ];
    }
}
