<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Classes;

use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\TicketOrderItem\TicketOrderItem;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;

class TicketOrderItemManager
{

    private TicketOrderItem $item;
    private int             $amount;

    public function __construct(TicketOrderItem $item, int $amount = 0)
    {
        $this->item   = $item;
        $this->amount = $amount;
    }

    public function getItem(): TicketOrderItem
    {
        return $this->item;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function addAmount(int $amount): void
    {
        $this->amount += $amount;
    }

}
