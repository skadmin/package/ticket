<?php

declare(strict_types=1);

namespace Skadmin\Ticket\Classes;

use Skadmin\Ticket\Doctrine\Ticket\Ticket;
use Skadmin\Ticket\Doctrine\TicketType\TicketType;

class TicketTypeManager
{

    private TicketType $ticketType;
    /** @var array<int, Ticket> */
    private array $tickets = [];

    public function __construct(TicketType $ticketType)
    {
        $this->ticketType = $ticketType;
    }

    public function clearTickets(): void
    {
        $this->tickets = [];
    }

    /**
     * @param array<Ticket> $tickets
     */
    public function setTickets(array $tickets): void
    {
        $this->clearTickets();
        foreach ($tickets as $ticket) {
            $this->addTicket($ticket);
        }
    }

    public function addTicket(Ticket $ticket): void
    {
        $this->tickets[$ticket->getSequence()] = $ticket;
    }

    public function getTicketType(): TicketType
    {
        return $this->ticketType;
    }

    /**
     * @return array<Ticket>
     */
    public function getTickets(): array
    {
        ksort($this->tickets);
        return $this->tickets;
    }

}
